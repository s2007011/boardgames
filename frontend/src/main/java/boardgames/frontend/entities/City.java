package boardgames.frontend.entities;

import java.util.List;
import java.util.Objects;

public class City {
    private int id;
    private String city;
    private String country;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        City city1 = (City) o;
        return id == city1.id &&
                Objects.equals(city, city1.city) &&
                Objects.equals(country, city1.country);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, city, country);
    }

    @Override
    public String toString() {
        return getCity() + ", " + getCountry();
    }

    public static int getIdFromString(List<City> cityList, String cityAndCountry) {
        for (City city : cityList) {
            if (cityAndCountry.equals(city.toString()))
                return city.getId();

        }

        return -1;
    }
}
