package boardgames.frontend.entities;

public class LikedReview {

    private Review review;

    public double getLiked() {
        return liked;
    }

    public void setLiked(double liked) {
        this.liked = liked;
    }

    private double liked;

    public Review getReview() {
        return review;
    }

    public void setReview(Review review) {
        this.review = review;
    }
}
