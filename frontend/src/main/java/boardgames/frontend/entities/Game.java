package boardgames.frontend.entities;

import java.util.Collection;
import java.util.List;

public class Game {

    private int id;
    private String name;
    private Integer minNoOfPlayers;
    private Integer maxNoOfPlayers;
    private String imageUrl;
    private List<Expansion> expansions;

    public List<Expansion> getExpansions() {
        return expansions;
    }

    public void setExpansions(List<Expansion> expansions) {
        this.expansions = expansions;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getMinNoOfPlayers() {
        return minNoOfPlayers;
    }

    public void setMinNoOfPlayers(Integer minNoOfPlayers) {
        this.minNoOfPlayers = minNoOfPlayers;
    }

    public Integer getMaxNoOfPlayers() {
        return maxNoOfPlayers;
    }

    public void setMaxNoOfPlayers(Integer maxNoOfPlayers) {
        this.maxNoOfPlayers = maxNoOfPlayers;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    @Override
    public String toString() {
        return id + ": " + name;
    }
}
