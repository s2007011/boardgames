package boardgames.frontend.entities;

public class Review {

    private int userId;
    private int expansionId;
    private String username;
    private Integer rating;
    private String description;

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public int getExpansionId() {
        return expansionId;
    }

    public void setExpansionId(int expansionId) {
        this.expansionId = expansionId;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public Integer getRating() {
        return rating;
    }

    public void setRating(Integer rating) {
        this.rating = rating;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
