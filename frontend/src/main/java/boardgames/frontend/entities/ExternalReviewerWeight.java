package boardgames.frontend.entities;

public class ExternalReviewerWeight {
    private int userId;
    private int externalReviewerId;
    private Integer weight;
    private ExternalReviewer externalReviewer;

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public int getExternalReviewerId() {
        return externalReviewerId;
    }

    public void setExternalReviewerId(int externalReviewerId) {
        this.externalReviewerId = externalReviewerId;
    }

    public Integer getWeight() {
        return weight;
    }

    public void setWeight(Integer weight) {
        this.weight = weight;
    }

    public ExternalReviewer getExternalReviewer() {
        return externalReviewer;
    }

    public void setExternalReviewer(ExternalReviewer externalReviewer) {
        this.externalReviewer = externalReviewer;
    }
}