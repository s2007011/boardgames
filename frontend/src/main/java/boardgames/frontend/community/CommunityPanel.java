package boardgames.frontend.community;

import javax.swing.*;
import java.awt.*;

public class CommunityPanel extends JPanel {

    public CommunityPanel() {
        setName("COMMUNITY");
        setLayout(new BorderLayout());

        add(new CommunitySidePanel(), BorderLayout.WEST);
        add(new CommunityMainPanel().getScrollPane(), BorderLayout.EAST);
    }
}
