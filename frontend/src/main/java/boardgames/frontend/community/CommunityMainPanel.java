package boardgames.frontend.community;

import boardgames.frontend.layouts.WrapLayout;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;
import java.util.List;
import java.util.stream.IntStream;

import static java.util.stream.Collectors.toList;

class CommunityMainPanel extends JPanel {

    private JScrollPane mainScrollPane;

    CommunityMainPanel() {

        int mainPanelPadding = 18;
        setLayout(new WrapLayout(WrapLayout.LEFT, mainPanelPadding, mainPanelPadding));
        setMinimumSize(new Dimension(880, 600));
        setMaximumSize(new Dimension(880, Integer.MAX_VALUE));
        setBackground(Color.white);


        new Thread(() -> {

            List<Entity> games = IntStream.range(0, 22)
                    .mapToObj(Entity::new)
                    .collect(toList());

            games.forEach(this::add);
            revalidate();

        }).start();

        configureMainScrollPane();
    }

    private void configureMainScrollPane() {

        mainScrollPane = new JScrollPane(this);
        mainScrollPane.setBorder(new EmptyBorder(0, 0, 0, 0));
        mainScrollPane.setPreferredSize(new Dimension(900, 600));
        mainScrollPane.getVerticalScrollBar().setUnitIncrement(16);
    }

    public JScrollPane getScrollPane() {
        return mainScrollPane;
    }
}
