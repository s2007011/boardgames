package boardgames.frontend.community;

import javax.imageio.ImageIO;
import javax.swing.*;
import javax.swing.border.Border;
import javax.swing.border.EmptyBorder;
import javax.swing.border.LineBorder;
import java.awt.*;
import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class Entity extends JPanel {

    static final Border border = new LineBorder(Color.gray, 1, true);
    final int id;
    final boolean group;
    boolean added = false;
    List<String> genres;

    public int getId() {
        return id;
    }

    static Image individualImage;
    static Image groupImage;

    static {
        try {
            groupImage = ImageIO.read(new File("images/group.png"));
            individualImage = ImageIO.read(new File("images/user.png"));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    Entity(int id) {
        this.id = id;
        setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));

        group = Math.random() > 0.8;

        setBackground(Color.lightGray);
        setPreferredSize(new Dimension(200, 300));
        setBorder(border);

        genres = Arrays.asList("Board Games", "Card Games");

        JLabel name = new JLabel("name" + id);
        name.setAlignmentX(CENTER_ALIGNMENT);
        name.setBorder(new EmptyBorder(190, 0, 0, 0));
        add(name);
        JLabel location = new JLabel("Edinburgh");
        location.setAlignmentX(CENTER_ALIGNMENT);
        add(location);
        JLabel genresLabel = new JLabel(genres.stream().collect(Collectors.joining(", ")));
        genresLabel.setAlignmentX(CENTER_ALIGNMENT);
        add(genresLabel);
        add(new JLabel("\n"));

        JButton button = new JButton(added ? "REMOVE" : "ADD");
        button.setAlignmentX(CENTER_ALIGNMENT);
        button.setBackground(Color.white);
        button.setOpaque(true);


        add(button);

        button.addActionListener(e -> {
            added = !added;
            button.setText(added ? "REMOVE" : "ADD");
            button.repaint();
        });
    }

    @Override
    public void paintComponent(Graphics g) {
        super.paintComponents(g);

        if (group) {
            g.drawImage(groupImage, 20, 20, 160, 160, null);
        } else {
            g.drawImage(individualImage, 20, 20, 160, 160, null);
        }
    }
}
