package boardgames.frontend.community;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;
import java.util.Arrays;
import java.util.List;

public class CommunitySidePanel extends JPanel {

    static final int padding = 20;

    CommunitySidePanel() {

        setBorder(new EmptyBorder(padding / 2, padding, 0, padding));
        setPreferredSize(new Dimension(300, 600));
        setBackground(Color.GRAY);
        setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));

        JLabel l1 = new JLabel("SORT BY");

        JRadioButton r1 = new JRadioButton("Name Ascending", true);
        JRadioButton r2 = new JRadioButton("Name Descending");

        ButtonGroup bg = new ButtonGroup();
        bg.add(r1);
        bg.add(r2);

        JLabel l2 = new JLabel("FILTERS");
        JTextField t1 = new JTextField();
        JCheckBox c2 = new JCheckBox("Individuals");
        JCheckBox c3 = new JCheckBox("Friends");
        JCheckBox c4 = new JCheckBox("Groups");

        JLabel l3 = new JLabel("LOCATION");
        String[] cities = new String[]{"Edinburgh, United Kingdom", "Glasgow, United Kingdom",
                "Aberdeen, United Kingdom", "Dundee, United Kingdom"};

        JComboBox<String> cb1 = new JComboBox<>(cities);

        JLabel l4 = new JLabel("GENRES");
        JCheckBox c5 = new JCheckBox("Board boardgames");
        JCheckBox c6 = new JCheckBox("Miniature war boardgames");
        JCheckBox c7 = new JCheckBox("Card boardgames");
        JCheckBox c8 = new JCheckBox("Role-playing boardgames");

        List<JComponent> list = Arrays.asList(l1, r1, r2, l2, t1, c2, c3, c4, l3, cb1, l4, c5, c6, c7, c8);

        t1.setMaximumSize(new Dimension(800, 36));

        list.forEach(component -> component.setBorder(new EmptyBorder(padding / 2, 0, padding / 2, 0)));
        list.forEach(this::add);

        for (JComponent component : list) {

            if (!(component instanceof JTextField) && !(component instanceof JComboBox))
                component.setBackground(Color.gray);

            component.setBorder(new EmptyBorder(padding / 2, 0, padding / 2, 0));
            add(component);
        }

        cb1.setAlignmentX(0);
        cb1.setMaximumSize(new Dimension(300, cb1.getPreferredSize().height));
    }
}
