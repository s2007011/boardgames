package boardgames.frontend.games;

import boardgames.frontend.entities.Game;
import boardgames.frontend.entities.Genre;

import javax.imageio.ImageIO;
import javax.swing.*;
import javax.swing.border.Border;
import javax.swing.border.EmptyBorder;
import javax.swing.border.LineBorder;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.IOException;
import java.net.URL;
import java.sql.Time;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static boardgames.frontend.GlobalStaticVariables.setSelectedExpansion;
import static boardgames.frontend.GlobalStaticVariables.setSelectedGame;
import static boardgames.frontend.games.GamesScrollPanel.expansionsMainPanel;
import static boardgames.frontend.games.GamesScrollPanel.expansionsSidePanel;

public class GamePanel extends JPanel {

    private static class Listener extends MouseAdapter {
        Thread timer;
        Map<Runnable, Boolean> shouldStop = new HashMap<>();

        @Override
        public void mousePressed(MouseEvent e) {

            GamePanel gamePanel = (GamePanel) e.getSource();

            if (!gamePanel.clickable)
                return;

            Container cardPanel = gamePanel.getParent().getParent().getParent().getParent().getParent();
            CardLayout cardLayout = (CardLayout) cardPanel.getLayout();

            cardLayout.show(cardPanel, "Expansions Panel");

            setSelectedGame(gamePanel.game);
            setSelectedExpansion(gamePanel.game.getExpansions().stream()
                    .filter(expansion -> expansion.getName().equals("Classic"))
                    .findFirst()
                    .orElse(null)
            );
            expansionsMainPanel.GetReviewsFast();

            expansionsSidePanel.refresh();

            for (Runnable runnable : shouldStop.keySet()) {
                shouldStop.replace(runnable, true);
                }
            shouldStop.clear();
            Runnable run;
            new Thread(run = new Runnable() {
                @Override
                public void run() {
                    while (!shouldStop.get(this)) {
                        try {
                            Thread.sleep(25000);
                        } catch (InterruptedException ex) {
                            ex.printStackTrace();
                        }
                        expansionsMainPanel.GetReviewsFast();
                        expansionsMainPanel.refresh();
                    }
                }
            }).start();
            shouldStop.put(run, false);
            expansionsMainPanel.refresh();
        }
    }

    static final Border border = new LineBorder(Color.gray, 1, true);
    private static Listener listener = new Listener();
    private Image image = null;

    private List<Genre> genres = new ArrayList<>();

    private final boolean clickable;
    private Game game;

    public Game getGame() {
        return game;
    }

    public List<Genre> getGenres() {
        return genres;
    }

    public GamePanel(Game game, boolean clickable) {

        setLayout(new BorderLayout());

        this.clickable = clickable;

        this.game = game;

        setName(game.getName());

        try {
            image = ImageIO.read(new URL(game.getImageUrl()));
        } catch (IOException e) {
            e.printStackTrace();
        }

        setBackground(Color.lightGray);
        setPreferredSize(new Dimension(200, 300));
        setBorder(border);
        addMouseListener(listener);

        JLabel label = new JLabel(game.getName());
        JPanel panel = new JPanel(new FlowLayout(FlowLayout.CENTER));
        panel.add(label);

        panel.setOpaque(false);
        panel.setBackground(Color.white);

        panel.setBorder(new EmptyBorder(240, 0, 0, 0));
        add(panel, BorderLayout.NORTH);
    }

    @Override
    public void paintComponent(Graphics g) {
        super.paintComponents(g);

        g.drawImage(image, 0, 0, 200, 240, null);
    }
}
