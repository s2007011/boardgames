package boardgames.frontend.games;

import boardgames.frontend.GlobalStaticVariables;
import boardgames.frontend.UserDetails;
import boardgames.frontend.entities.LikedReview;
import boardgames.frontend.entities.Review;
import boardgames.frontend.layouts.WrapLayout;
import boardgames.frontend.procedures.GetExternalAndUserRatings;
import boardgames.frontend.procedures.GetTopNUserReviews;
import boardgames.frontend.procedures.entities.ExternalAndUserRatings;
import boardgames.frontend.procedures.entities.TopNUserReviews;
import boardgames.frontend.requests.PostRequest;
import com.fasterxml.jackson.core.JsonProcessingException;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static boardgames.frontend.GlobalStaticVariables.getSelectedExpansion;
import static boardgames.frontend.GlobalStaticVariables.getServerUrl;
import static boardgames.frontend.UserDetails.getOwnedExpansionIds;
import static boardgames.frontend.UserDetails.getUser;

class ExpansionsMainPanel extends JPanel {

    private static class Listener implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent e) {

            Integer selectedExpansionId = getSelectedExpansion().getId();

            if (getOwnedExpansionIds().contains(selectedExpansionId)) {

                String response = new PostRequest().send(getServerUrl() + "users/removeExpansion", Map.of(
                        "userId", getUser().getId(),
                        "expansionId", getSelectedExpansion().getId()));

                if (response.equals("Success")) {
                    getOwnedExpansionIds().remove(selectedExpansionId);
                    button.setText("Add expansion");
                    button.getParent().repaint();
                } else {
                    System.err.println("Error: " + response);
                }

                GameListener.checkCheckBoxes();
            } else {

                String response = new PostRequest().send(getServerUrl() + "users/addExpansion", Map.of(
                        "userId", getUser().getId(),
                        "expansionId", getSelectedExpansion().getId()));

                if (response.equals("Success")) {
                    getOwnedExpansionIds().add(getSelectedExpansion().getId());
                    button.setText("Remove expansion");
                } else {
                    System.err.println("Error: " + response);
                }
            }
        }

    }

    private JScrollPane mainScrollPane;
    private JLabel ratings = new JLabel("No average available");

    private static JButton button = new JButton();
    private static Listener listener = new Listener();

    ExpansionsMainPanel() {

        int mainPanelPadding = 18;
        setLayout(new WrapLayout(WrapLayout.LEFT, mainPanelPadding, mainPanelPadding));
        setMinimumSize(new Dimension(880, 600));
        setMaximumSize(new Dimension(880, Integer.MAX_VALUE));
        setBackground(Color.white);

        configureMainScrollPane();

        button.addActionListener(listener);
    }

    private void configureMainScrollPane() {

        mainScrollPane = new JScrollPane(this);
        mainScrollPane.setBorder(new EmptyBorder(0, 0, 0, 0));
        mainScrollPane.setPreferredSize(new Dimension(900, 600));
        mainScrollPane.getVerticalScrollBar().setUnitIncrement(16);
    }

    public JScrollPane getScrollPane() {
        return mainScrollPane;
    }

    private List<ExternalAndUserRatings> externalAndUserRatings;
    private List<TopNUserReviews> topReviews;
    private List<LikedReview> likedReviews = new ArrayList<>();
    private List<Review> reviews = new ArrayList<>();
    private List<Review> externalReviews = new ArrayList<>();

    private void GetExternalAndUserRatings() {

        externalAndUserRatings = GetExternalAndUserRatings
                .getProcedureResults(getSelectedExpansion().getId(), getUser().getId());

    }

    private double GetWeightedAverage() {

        double average = 0;
        int divisor = 0;
        for (ExternalAndUserRatings externalAndUserRating : externalAndUserRatings) {
            average += externalAndUserRating.getRating() * externalAndUserRating.getWeight();
            divisor += externalAndUserRating.getWeight();
        }
        average = average / (divisor);
        return average;
    }

    public void GetReviews() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                externalReviews.clear();
                GetExternalAndUserRatings();
                if (externalAndUserRatings.size() > 1)
                    for (ExternalAndUserRatings externalAndUserRating : externalAndUserRatings) {
                        if (externalAndUserRating.getName().equals("users"))
                            continue;
                        Review rev = new Review();
                        rev.setDescription(externalAndUserRating.getReview_description());
                        rev.setRating((int) externalAndUserRating.getRating());
                        rev.setUserId(-1);
                        rev.setUsername(externalAndUserRating.getName());
                        rev.setExpansionId(getSelectedExpansion().getId());
                        externalReviews.add(rev);
                    }
                if (externalAndUserRatings.size() > 0) {
                    ratings.setText("User average:"
                            + String.format("%.2f", externalAndUserRatings.get(externalAndUserRatings.size() - 1).getRating())
                            + " | Weighted average:" + String.format("%.2f", GetWeightedAverage()));
                }
//                refresh();
            }
        }).start();

        likedReviews.clear();
        try {
            topReviews = GetTopNUserReviews.getProcedureResults(getSelectedExpansion().getId(), 3);
            for (TopNUserReviews topReview : topReviews) {
                Review rev = new Review();
                rev.setUserId(topReview.getUser_id());
                rev.setExpansionId(getSelectedExpansion().getId());
                rev.setUsername(topReview.getUsername());
                rev.setDescription(topReview.getReview_description());
                rev.setRating(topReview.getRating());
                LikedReview likedReview = new LikedReview();
                likedReview.setReview(rev);
                likedReview.setLiked(topReview.getLikeavg());
                likedReviews.add(likedReview);
            }
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        reviews.clear();
        reviews = GlobalStaticVariables.getReviewPanels().stream()
                .filter(reviewPanel -> reviewPanel.getReview().getExpansionId() == getSelectedExpansion().getId())
                .map(ReviewPanel::getReview)
                .collect(Collectors.toList());

        while (reviews.size() > 3)
            reviews.remove(3);
    }

    public void GetReviewsFast() {

        while (GlobalStaticVariables.allExternalAndUser.get(getSelectedExpansion().getId()) == null) ;
        externalAndUserRatings = GlobalStaticVariables.allExternalAndUser.get(getSelectedExpansion().getId());
        externalReviews.clear();
        if (externalAndUserRatings.size() > 1)
            for (ExternalAndUserRatings externalAndUserRating : externalAndUserRatings) {
                if (externalAndUserRating.getName().equals("users"))
                    continue;
                Review rev = new Review();
                rev.setDescription(externalAndUserRating.getReview_description());
                rev.setRating((int) externalAndUserRating.getRating());
                rev.setUserId(-1);
                rev.setUsername(externalAndUserRating.getName());
                rev.setExpansionId(getSelectedExpansion().getId());
                externalReviews.add(rev);
            }
        if (externalAndUserRatings.size() > 0) {
            ratings.setText("User average:"
                    + String.format("%.2f", externalAndUserRatings.get(externalAndUserRatings.size() - 1).getRating())
                    + " | Weighted average:" + String.format("%.2f", GetWeightedAverage()));
        }

        likedReviews.clear();

        topReviews = GlobalStaticVariables.allTopReviews.get(getSelectedExpansion().getId());
        for (TopNUserReviews topReview : topReviews) {
            Review rev = new Review();
            rev.setUserId(topReview.getUser_id());
            rev.setExpansionId(getSelectedExpansion().getId());
            rev.setUsername(topReview.getUsername());
            rev.setDescription(topReview.getReview_description());
            rev.setRating(topReview.getRating());
            LikedReview likedReview = new LikedReview();
            likedReview.setReview(rev);
            likedReview.setLiked(topReview.getLikeavg());
            likedReviews.add(likedReview);
        }

        reviews.clear();
        reviews = GlobalStaticVariables.getReviewPanels().stream()
                .filter(reviewPanel -> reviewPanel.getReview().getExpansionId() == getSelectedExpansion().getId())
                .map(ReviewPanel::getReview)
                .collect(Collectors.toList());

        while (reviews.size() > 3)
            reviews.remove(3);
    }

    private Review dummy;

    private Review GetDummyReview() {
        if (dummy != null) {
            return dummy;
        }
        Review rev = new Review();
        rev.setRating(3);
        rev.setDescription("Waiting for original reviews if they exist, this may take 10-20 seconds");
        rev.setUsername("Waiting for reviews");
        rev.setExpansionId(getSelectedExpansion().getId());
        rev.setUserId(UserDetails.getUser().getId());
        dummy = rev;
        myReviewPanel = new MyReviewPanel(dummy);
        return rev;
    }

    private MyReviewPanel myReviewPanel;
    private List<ReviewPanel> likedReviewPanels = new ArrayList<>();
    private List<ReviewPanel> noLikedreviewPanels = new ArrayList<>();
    private List<ExternalReviewPanel> externalReviewPanels = new ArrayList<>();

    public void refresh() {
        removeAll();
        //Dummy review is used to submit the user reviews. Updates the expnansion id to the current one.
        GetDummyReview().setExpansionId(getSelectedExpansion().getId());
        if (getOwnedExpansionIds() == null) synchronized (UserDetails.getInstance()) {
            try {
                UserDetails.getInstance().wait();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        if (getOwnedExpansionIds().contains(getSelectedExpansion().getId())) {
            button.setText("Remove expansion");
        } else {
            button.setText("Add expansion");
        }
        add(ratings);
        add(button);
        add(myReviewPanel);
        for (LikedReview review : likedReviews) {
            add(new ReviewPanel(review.getReview(), review.getLiked()));
        }
        for (Review review : reviews.stream()
                .filter(rev -> !likedReviews.stream()
                        .anyMatch(likedReview -> likedReview.getReview().getUserId() == rev.getUserId()))
                .collect(Collectors.toList())) {
            add(new ReviewPanel(review));
        }
        for (Review externalReview : externalReviews) {
            add(new ExternalReviewPanel(externalReview));
        }
        revalidate();
        repaint();
    }



}


//    add(myReviewPanel);
////        if (likedReviews.size() == 0 && reviews.size() == 0) {
////            add(new ReviewPanel(GetDummyReview()));
////        }
//        likedReviewPanels.clear();
//                for (LikedReview review : likedReviews) {
//                ReviewPanel rev;
//                likedReviewPanels.add(rev=new ReviewPanel(review.getReview(), review.getLiked()));
//                add(rev);
//                }
//                noLikedreviewPanels.clear();
//                for (Review review : reviews.stream()
//                .filter(rev -> !likedReviews.stream()
//                .anyMatch(likedReview -> likedReview.getReview().getUserId() == rev.getUserId()))
//                .collect(Collectors.toList())) {
//                ReviewPanel rev;
//                add(rev=new ReviewPanel(review));
//                noLikedreviewPanels.add(rev);
//                }
//                externalReviewPanels.clear();
//                for (Review externalReview : externalReviews) {
//                ExternalReviewPanel rev;
//                add(rev=new ExternalReviewPanel(externalReview));
//                externalReviewPanels.add(rev);
//                }