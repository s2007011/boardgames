package boardgames.frontend.games;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Stream;

import static boardgames.frontend.GlobalStaticVariables.getGenres;
import static boardgames.frontend.GlobalStaticVariables.getServerUrl;
import static java.util.stream.Collectors.toList;

class GamesSidePanel extends JPanel {

    JLabel l1 = new JLabel("SORT BY");

    static JRadioButton nameAscending = new JRadioButton("Name Ascending", true);
    static JRadioButton nameDescending = new JRadioButton("Name Descending");

    static ButtonGroup buttonGroup = new ButtonGroup();
    List<JRadioButton> radioButtons = Arrays.asList(nameAscending, nameDescending);

    static JLabel filtersLabel = new JLabel("FILTERS");
    static JTextField textField = new JTextField();
    static JCheckBox myGames = new JCheckBox("My games");
    static JCheckBox notOwnedGames = new JCheckBox("Not owned games");
    static JLabel genresLabel = new JLabel("GENRES");


    static List<JCheckBox> checkBoxes;

    static final int padding = 20;

    static GameListener gameListener = new GameListener();

    GamesSidePanel() {

        setBorder(new EmptyBorder(padding / 2, padding, padding / 2, padding));
        setPreferredSize(new Dimension(300, 600));
        setBackground(Color.GRAY);
        setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));

        radioButtons.forEach(buttonGroup::add);

        textField.setMaximumSize(new Dimension(800, 36));


        if (getGenres() == null) synchronized (getServerUrl()) {
            try {
                getServerUrl().wait();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        checkBoxes = getGenres().stream()
                .map(genre -> new JCheckBox(genre.getName()))
                .collect(toList());


        List<JComponent> components = Arrays.asList(l1, nameAscending, nameDescending, filtersLabel, textField,
                myGames, notOwnedGames, genresLabel);

        components = Stream.concat(components.stream(), checkBoxes.stream())
                .collect(toList());

        checkBoxes.forEach(checkBox -> checkBox.addActionListener(gameListener));
        nameAscending.addActionListener(gameListener);
        nameDescending.addActionListener(gameListener);
        myGames.addActionListener(gameListener);
        notOwnedGames.addActionListener(gameListener);
        textField.addKeyListener(gameListener);

        for (JComponent component : components) {

            if (!(component instanceof JTextField))
                component.setBackground(Color.gray);

            component.setBorder(new EmptyBorder(padding / 2, 0, padding / 2, 0));
            add(component);
        }
    }
}
