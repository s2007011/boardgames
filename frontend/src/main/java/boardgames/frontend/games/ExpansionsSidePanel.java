package boardgames.frontend.games;

import javax.imageio.ImageIO;
import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static boardgames.frontend.GlobalStaticVariables.*;
import static boardgames.frontend.games.GamesScrollPanel.expansionsMainPanel;

public class ExpansionsSidePanel extends JPanel {

    class Listener implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent e) {


            Object source = e.getSource();

            if (source == back) {

                Container parent = getParent().getParent();
                CardLayout cardLayout = (CardLayout) parent.getLayout();
                cardLayout.show(parent, "Games Panel");
                refresh();
                return;
            } else {

                JButton sourceButton = (JButton) source;
                setSelectedExpansion(
                        getSelectedGame().getExpansions().stream()
                                .filter(expansion -> expansion.getName().equals(sourceButton.getText()))
                                .findFirst()
                                .orElse(null));
                expansionsMainPanel.GetReviewsFast();

            }

            refresh();
            expansionsMainPanel.refresh();
            expansionsMainPanel.GetReviewsFast();
            expansionsMainPanel.refresh();
        }
    }

    static final int padding = 20;
    final Listener listener = new Listener();
    static JButton back = null;


    ExpansionsSidePanel() {

        setBorder(new EmptyBorder(padding / 2, padding, padding / 2, padding));
        setPreferredSize(new Dimension(300, 600));
        setBackground(Color.GRAY);
        setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));

        try {
            Image backImage = ImageIO.read(new File("images/backButton.png"));
            backImage = backImage.getScaledInstance(40, 40, Image.SCALE_DEFAULT);
            back = new JButton(new ImageIcon(backImage));
            back.addActionListener(listener);
        } catch (IOException e) {
            e.printStackTrace();
        }

        back.setBorder(new EmptyBorder(padding / 2, 0, padding / 2, 0));
        add(back);
    }

    public void refresh() {

        removeAll();

        List<JButton> expansionButtons = getSelectedGame().getExpansions().stream()
                .map(expansion -> new JButton(expansion.getName()))
                .collect(Collectors.toList());

        expansionButtons = Stream.concat(Stream.of(back), expansionButtons.stream())
                .collect(Collectors.toList());

        for (JButton button : expansionButtons) {
            button.setBorder(new EmptyBorder(padding / 2, 0, padding / 2, 0));
            add(button);
            button.addActionListener(listener);

            if (getSelectedExpansion().getName().equals(button.getText())) {
                button.setForeground(Color.darkGray);
            } else {
                button.setForeground(Color.black);
            }
        }

        revalidate();
        repaint();
    }
}
