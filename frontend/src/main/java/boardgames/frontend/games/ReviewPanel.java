package boardgames.frontend.games;

import boardgames.frontend.GlobalStaticVariables;
import boardgames.frontend.UserDetails;
import boardgames.frontend.entities.Review;
import boardgames.frontend.procedures.LikeForReviewCalls;

import javax.swing.*;
import javax.swing.border.Border;
import javax.swing.border.LineBorder;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class ReviewPanel extends JPanel {

    private static final Border border = new LineBorder(Color.gray, 1, true);

    private final Review review;
    private JButton likeButton;

    public JButton getLikeButton() {
        return likeButton;
    }

    public void setLikeButton(JButton likeButton) {
        this.likeButton = likeButton;
    }

    public JButton getDislikeButton() {
        return dislikeButton;
    }

    public void setDislikeButton(JButton dislikeButton) {
        this.dislikeButton = dislikeButton;
    }

    private JButton dislikeButton;
    private JLabel likeAverage = new JLabel();

    public ReviewPanel(Review review) {
        this.review = review;
        setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));

        setBackground(Color.lightGray);
        setPreferredSize(new Dimension(860, 125));
        setBorder(border);

        JLabel header = new JLabel("Reviewer: " + review.getUsername() +
                " - Rating: " + review.getRating().toString() + " / 5");

        add(header);
        add(new JLabel("\n\n"));
        add(new JLabel("Description: " + review.getDescription()));
        likeButton = new JButton("like");

        likeButton.setPreferredSize(new Dimension(200, 100));
        add(likeButton);
        dislikeButton = new JButton("dislike");
        likeButton.setPreferredSize(new Dimension(200, 100));
        add(dislikeButton);

        int isLiked = -1;
        isLiked = LikeForReviewCalls.userLikesReview(
                UserDetails.getInstance().getId(),
                review.getUserId(),
                review.getExpansionId());
        if (isLiked == 1)
            dislikeButton.setEnabled(false);
        if (isLiked == 2)
            likeButton.setEnabled(false);
//        if (isLiked == 0)


        add(likeAverage);
        likeButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                GlobalStaticVariables.requestUpdateOfExpansionProcedures(review.getExpansionId());
                LikeForReviewCalls.addLike(UserDetails.getInstance().getId(),
                        review.getUserId(),
                        review.getExpansionId(), 1);
                likeButton.setEnabled(false);
                dislikeButton.setEnabled(true);
            }
        });
        dislikeButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                GlobalStaticVariables.requestUpdateOfExpansionProcedures(review.getExpansionId());
                LikeForReviewCalls.addLike(UserDetails.getInstance().getId(),
                        review.getUserId(),
                        review.getExpansionId(), 0);
                likeButton.setEnabled(true);
                dislikeButton.setEnabled(false);
            }
        });
    }

    public ReviewPanel(Review review, Double likes) {
        this(review);
        likeAverage.setText("Like average:" + String.format("%.2f", likes) + "/1");
    }

    public Review getReview() {
        return review;
    }
}
