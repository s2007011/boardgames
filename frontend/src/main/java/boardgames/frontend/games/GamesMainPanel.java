package boardgames.frontend.games;

import boardgames.frontend.layouts.WrapLayout;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;

import static boardgames.frontend.GlobalStaticVariables.getGamePanels;
import static boardgames.frontend.GlobalStaticVariables.getServerUrl;
import static boardgames.frontend.games.GamesSidePanel.gameListener;

class GamesMainPanel extends JPanel {

    private JScrollPane mainScrollPane;

    GamesMainPanel() {

        gameListener.setGamesMainPanel(this);

        int mainPanelPadding = 18;
        setLayout(new WrapLayout(WrapLayout.LEFT, mainPanelPadding, mainPanelPadding));
        setMinimumSize(new Dimension(880, 600));
        setMaximumSize(new Dimension(880, Integer.MAX_VALUE));
        setBackground(Color.white);

        try {

            if (getGamePanels() == null) synchronized (getServerUrl()) {
                getServerUrl().wait();
            }


            getGamePanels().forEach(this::add);
        } catch (InterruptedException e) {
            e.printStackTrace();
            System.exit(-1);
        }

        revalidate();

        configureMainScrollPane();
    }

    private void configureMainScrollPane() {

        mainScrollPane = new JScrollPane(this);
        mainScrollPane.setBorder(new EmptyBorder(0, 0, 0, 0));
        mainScrollPane.setPreferredSize(new Dimension(900, 600));
        mainScrollPane.getVerticalScrollBar().setUnitIncrement(16);
    }

    public JScrollPane getScrollPane() {
        return mainScrollPane;
    }
}
