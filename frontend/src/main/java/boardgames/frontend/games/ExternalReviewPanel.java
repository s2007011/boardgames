package boardgames.frontend.games;

import boardgames.frontend.UserDetails;
import boardgames.frontend.entities.Review;
import boardgames.frontend.procedures.LikeForReviewCalls;

import javax.swing.*;
import javax.swing.border.Border;
import javax.swing.border.LineBorder;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class ExternalReviewPanel extends JPanel {

    private static final Border border = new LineBorder(Color.gray, 1, true);

    private final Review review;
    private JButton likeButton;

    public JButton getLikeButton() {
        return likeButton;
    }

    public void setLikeButton(JButton likeButton) {
        this.likeButton = likeButton;
    }

    public JButton getDislikeButton() {
        return dislikeButton;
    }

    public void setDislikeButton(JButton dislikeButton) {
        this.dislikeButton = dislikeButton;
    }

    private JButton dislikeButton;

    public ExternalReviewPanel(Review review) {
        this.review = review;
        setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));

        setBackground(Color.lightGray);
        setPreferredSize(new Dimension(860, 100));
        setBorder(border);

        JLabel header = new JLabel("External Reviewer: " + review.getUsername() +
                " - Rating: " +  review.getRating().toString() + " / 5");

        add(header);
        add(new JLabel("\n\n"));
        add(new JLabel("Description: " + review.getDescription()));

    }

    public Review getReview() {
        return review;
    }
}