package boardgames.frontend.games;

import javax.swing.*;
import java.awt.*;

public class GamesScrollPanel extends JPanel {

    static class GamesPanel extends JPanel {

        GamesPanel() {
            setName("Games Panel");

            setLayout(new BorderLayout());

            add(new GamesSidePanel(), BorderLayout.WEST);
            add(new GamesMainPanel().getScrollPane(), BorderLayout.EAST);
        }
    }

    static ExpansionsSidePanel expansionsSidePanel = new ExpansionsSidePanel();
    static ExpansionsMainPanel expansionsMainPanel = new ExpansionsMainPanel();

    static class ExpansionsPanel extends JPanel {

        ExpansionsPanel() {
            setName("Expansions Panel");

            setLayout(new BorderLayout());

            add(expansionsSidePanel, BorderLayout.WEST);
            add(expansionsMainPanel.getScrollPane(), BorderLayout.EAST);
        }
    }

    public GamesScrollPanel() {
        setName("GAMES");

        CardLayout cardLayout = new CardLayout();
        setLayout(cardLayout);

        GamesPanel gamesPanel = new GamesPanel();
        ExpansionsPanel expansionsPanel = new ExpansionsPanel();
        add(gamesPanel.getName(), gamesPanel);
        add(expansionsPanel.getName(), expansionsPanel);
    }
}
