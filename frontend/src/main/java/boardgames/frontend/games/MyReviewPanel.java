package boardgames.frontend.games;

import boardgames.frontend.GlobalStaticVariables;
import boardgames.frontend.UserDetails;
import boardgames.frontend.entities.Review;
import boardgames.frontend.procedures.LikeForReviewCalls;

import javax.swing.*;
import javax.swing.border.Border;
import javax.swing.border.LineBorder;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Optional;
import java.util.stream.Collectors;

public class MyReviewPanel extends JPanel {

    private static final Border border = new LineBorder(Color.gray, 1, true);

    private final Review review;
    private JButton likeButton;

    public JButton getLikeButton() {
        return likeButton;
    }

    public void setLikeButton(JButton likeButton) {
        this.likeButton = likeButton;
    }

    public JButton getDislikeButton() {
        return dislikeButton;
    }

    public void setDislikeButton(JButton dislikeButton) {
        this.dislikeButton = dislikeButton;
    }

    private JButton dislikeButton;

    public MyReviewPanel(Review review) {
        this.review = review;
        setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));

        setBackground(Color.lightGray);
        setPreferredSize(new Dimension(860, 250));
        setBorder(border);
        JLabel header = new JLabel("Add/Update your review");

        add(header);
        add(new JLabel("\n\n"));
        add(new JLabel("Description: "));
        JTextField description=new JTextField();
        add(description);
        JButton submit=new JButton("Submit");

        Optional<Review> firstReviewPanel = GlobalStaticVariables.getReviewPanels().stream().filter(reviewPanel -> reviewPanel.getReview().getUserId()
                == review.getUserId() && reviewPanel.getReview().getExpansionId() == review.getExpansionId())
                .map(ReviewPanel::getReview)
                .findFirst();
        int selected=2;
        if(firstReviewPanel.isPresent()){
            Review rev=firstReviewPanel.get();
            description.setText(rev.getDescription());
            if(rev.getRating()!=null)
                selected=rev.getRating()-1;
        }

        ButtonGroup bg =new ButtonGroup();
        for (int i = 0; i <5 ; i++) {
            JRadioButton rb =	new JRadioButton(i+1+"");
            rb.setActionCommand(i+1+"");
            if(i==selected)
                rb.setSelected(true);
            else rb.setSelected(false);
            bg.add(rb);
            add(rb);
        }

//        if(firstReviewPanel.isEmpty())
//            submit.setEnabled(false);
        submit.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                GlobalStaticVariables.requestUpdateOfExpansionProcedures(review.getExpansionId());
                LikeForReviewCalls.addReview(UserDetails.getInstance().getId(),review.getExpansionId(),
                        Integer.parseInt(bg.getSelection().getActionCommand()),description.getText());
                 if(firstReviewPanel.isPresent()) {
                     firstReviewPanel.get().setRating(Integer.parseInt(bg.getSelection().getActionCommand()));
                     firstReviewPanel.get().setDescription(description.getText());
                 }

            }
        });
        add(submit);
    }

    public Review getReview() {
        return review;
    }
}
