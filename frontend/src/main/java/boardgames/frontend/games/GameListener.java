package boardgames.frontend.games;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.util.Comparator;
import java.util.List;

import static boardgames.frontend.GlobalStaticVariables.getGamePanels;
import static boardgames.frontend.UserDetails.getOwnedGameIds;
import static boardgames.frontend.games.GamesSidePanel.*;

public class GameListener extends KeyAdapter implements ActionListener {

    public void setGamesMainPanel(GamesMainPanel gamesMainPanel) {
        GameListener.gamesMainPanel = gamesMainPanel;
    }

    private static GamesMainPanel gamesMainPanel;

    @Override
    public void actionPerformed(ActionEvent e) {
        checkCheckBoxes();
    }

    @Override
    public void keyReleased(KeyEvent e) {
        checkCheckBoxes();
    }

    public static void checkCheckBoxes() {

        gamesMainPanel.removeAll();
        gamesMainPanel.revalidate();
        gamesMainPanel.repaint();

        if (gamesMainPanel == null) return;

        var gamePanels = getGamePanels().stream();

        if (nameAscending.isSelected()) {
            gamePanels = gamePanels.sorted(Comparator.comparing(panel -> panel.getGame().getName()));
        } else {
            gamePanels = gamePanels.sorted((p1, p2) -> p2.getGame().getName().compareTo(p1.getGame().getName()));
        }

        boolean selectedGenreBoxes = false;

        for (JCheckBox checkBox : checkBoxes) {
            if (checkBox.isSelected()) {
                selectedGenreBoxes = true;
                break;
            }
        }

        if (selectedGenreBoxes) {
            for (JCheckBox checkBox : checkBoxes) {

                if (!checkBox.isSelected()) {
                    gamePanels = gamePanels
                            .filter(gamePanel -> !gamePanel.getGenres().get(0).getName().equals(checkBox.getText()));
                }
            }
        }

        if (myGames.isSelected() && !notOwnedGames.isSelected()
                || !myGames.isSelected() && notOwnedGames.isSelected()) {

            List<Integer> ownedGameIds = getOwnedGameIds();

            if (myGames.isSelected())
                gamePanels = gamePanels
                        .filter(gamePanel -> ownedGameIds.contains(gamePanel.getGame().getId()));

            if (notOwnedGames.isSelected()) {
                gamePanels = gamePanels
                        .filter(gamePanel -> !ownedGameIds.contains(gamePanel.getGame().getId()));
            }
        }

        if (!textField.getText().equals(""))
            gamePanels = gamePanels.filter(gamePanel -> gamePanel.getName().toLowerCase()
                    .contains(textField.getText().toLowerCase()));

        gamePanels.forEach(gamesMainPanel::add);

        gamesMainPanel.revalidate();
        gamesMainPanel.repaint();
    }
}
