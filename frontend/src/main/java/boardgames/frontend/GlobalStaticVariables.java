package boardgames.frontend;

import boardgames.frontend.entities.*;
import boardgames.frontend.games.GamePanel;
import boardgames.frontend.games.ReviewPanel;
import boardgames.frontend.procedures.GetExternalAndUserRatings;
import boardgames.frontend.procedures.GetTopNUserReviews;
import boardgames.frontend.procedures.entities.ExternalAndUserRatings;
import boardgames.frontend.procedures.entities.TopNUserReviews;
import boardgames.frontend.requests.GetRequest;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static java.util.stream.Collectors.toList;

public class GlobalStaticVariables {

    public GlobalStaticVariables() {
        new Thread(GlobalStaticVariables::initialise).start();
    }

    public static Map<Integer, Integer> expansionUpdateProcedures = new HashMap<>();

    public static Map<Integer, List<TopNUserReviews>> allTopReviews = new HashMap<>();

    public static Map<Integer, List<ExternalAndUserRatings>> allExternalAndUser = new HashMap<>();

    /**
     * Do not use, until better database can be found
     *
     * @param eid
     */
    public static void requestUpdateOfExpansionProcedures(int eid) {
        expansionUpdateProcedures.replace(eid, 1);

        new Thread(new Runnable() {
            @Override
            public void run() {
                ObjectMapper mapper = getObjectMapper();
                String reviewsJson = new GetRequest().send(serverUrl + "reviews/all", null);
                List<Review> reviews = null;
                try {
                    reviews = mapper.readValue(reviewsJson, new TypeReference<>() {
                    });
                } catch (JsonProcessingException e) {
                    e.printStackTrace();
                }
                reviewPanels = reviews.stream()
                        .map(ReviewPanel::new)
                        .collect(toList());

            }
        }).start();
    }

    /**
     * Do not use,until better database can be found
     */
    private static void generateProcedureWorkers() {
        for (Game game : games) {
            for (Expansion expansion : game.getExpansions()) {
                expansionUpdateProcedures.put(expansion.getId(), 1);
                allTopReviews.put(expansion.getId(), null);
                allExternalAndUser.put(expansion.getId(), null);
                new Thread(new Runnable() {
                    @Override
                    public void run() {

                        while (true) {
                            while (expansionUpdateProcedures.get(expansion.getId()) == null) {
                                try {
                                    Thread.sleep(1000);
                                } catch (InterruptedException e) {
                                    e.printStackTrace();
                                }
                            }
                            expansionUpdateProcedures.replace(expansion.getId(), null);
                            allTopReviews.replace(expansion.getId(), null);
                            allExternalAndUser.replace(expansion.getId(), null);
                            try {
                                allTopReviews.replace(expansion.getId(), GetTopNUserReviews.getProcedureResults(expansion.getId(), 3));
                                allExternalAndUser.replace(expansion.getId(), GetExternalAndUserRatings.getProcedureResults(expansion.getId(),
                                        UserDetails.getInstance().getId()));

                            } catch (JsonProcessingException e) {
                                e.printStackTrace();
                            }
                            System.out.println("Finished " + expansion.getId() + " procedures!");
                        }
                    }
                }).start();
            }
        }
    }

    public static void initialise() {

        String gamesJson = new GetRequest().send(serverUrl + "games/all", null);
        String genresJson = new GetRequest().send(serverUrl + "genres/all", null);
        String gameGenresJson = new GetRequest().send(serverUrl + "genres/ofGames", null);
        String citiesJson = new GetRequest().send(serverUrl + "cities/all", null);
        String reviewsJson = new GetRequest().send(serverUrl + "reviews/all", null);

        try {
            ObjectMapper mapper = getObjectMapper();

            games = mapper.readValue(gamesJson, new TypeReference<>() {
            });
            generateProcedureWorkers();

            genres = mapper.readValue(genresJson, new TypeReference<>() {
            });
            gameGenres = mapper.readValue(gameGenresJson, new TypeReference<>() {
            });
            cities = mapper.readValue(citiesJson, new TypeReference<>() {
            });

            setSelectedGame(games.get(0));
            setSelectedExpansion(games.get(0).getExpansions().get(0));


            gamePanels = games.stream()
                    .map(game -> new GamePanel(game, true))
                    .sorted(Comparator.comparing(panel -> panel.getGame().getName()))
                    .collect(toList());

            List<Review> reviews = mapper.readValue(reviewsJson, new TypeReference<>() {
            });
            reviewPanels = reviews.stream()
                    .map(ReviewPanel::new)
                    .collect(toList());

            for (GameGenre gameGenre : gameGenres) {

                Genre selectedGenre = null;
                GamePanel selectedGamePanel = null;

                for (Genre genre : genres) {
                    if (genre.getId() == gameGenre.getGenreId())
                        selectedGenre = genre;
                }

                for (GamePanel gamePanel : gamePanels) {
                    if (gamePanel.getGame().getId() == gameGenre.getGameId())
                        selectedGamePanel = gamePanel;
                }

                selectedGamePanel.getGenres().add(selectedGenre);
            }

            synchronized (getServerUrl()) {
                serverUrl.notifyAll();
            }

        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
    }

    public static ObjectMapper getObjectMapper() {
        return objectMapper;
    }

    public static List<Genre> getGenres() {
        return genres;
    }

    public static List<Game> getGames() {
        return games;
    }

    public static List<GamePanel> getGamePanels() {
        return gamePanels;
    }

    public static String getServerUrl() {
        return serverUrl;
    }

    public static Expansion getSelectedExpansion() {
        return selectedExpansion;
    }

    public static void setSelectedExpansion(Expansion selectedExpansion) {
        GlobalStaticVariables.selectedExpansion = selectedExpansion;
    }

    public static Game getSelectedGame() {
        return selectedGame;
    }

    public static void setSelectedGame(Game selectedGame) {
        GlobalStaticVariables.selectedGame = selectedGame;
    }

    public static List<GameGenre> getGameGenres() {
        return gameGenres;
    }

    public static List<City> getCities() {
        return cities;
    }

    public static List<ReviewPanel> getReviewPanels() {
        return reviewPanels;
    }

    private static final String serverUrl = "http://localhost/";
    private static final ObjectMapper objectMapper = new ObjectMapper();
    private static List<Genre> genres;
    private static List<Game> games;
    private static List<GamePanel> gamePanels;
    private static List<GameGenre> gameGenres;
    private static List<City> cities;
    private static List<ReviewPanel> reviewPanels;

    private static Expansion selectedExpansion;
    private static Game selectedGame;
}
