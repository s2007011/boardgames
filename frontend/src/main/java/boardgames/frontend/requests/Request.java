package boardgames.frontend.requests;

import java.net.http.HttpClient;
import java.util.Map;

public abstract class Request {
    private static final HttpClient httpClient = HttpClient.newBuilder()
            .version(HttpClient.Version.HTTP_2)
            .build();

    public HttpClient getHttpClient() {
        return httpClient;
    }

    public static String getStringFromMap(Map<Object, Object> parameters) {

        String string = "";

        if (parameters == null) return string;

        for (Map.Entry<Object, Object> entry : parameters.entrySet()) {
            if (!string.equals("")) string += "&";

            string += entry.getKey() + "=" + entry.getValue();
        }

        return string;
    }

    public abstract String send(String url, Map<Object, Object> parameters);
}
