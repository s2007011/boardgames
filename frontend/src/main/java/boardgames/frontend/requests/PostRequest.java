package boardgames.frontend.requests;

import java.io.IOException;
import java.net.URI;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.util.Map;

import static java.net.http.HttpRequest.BodyPublishers.ofString;

public class PostRequest extends Request {

    public String send(String url, Map<Object, Object> parameters) {


        HttpRequest request = HttpRequest.newBuilder()
                .POST(ofString(getStringFromMap(parameters)))
                .uri(URI.create(url))
                .setHeader("User-Agent", "Java 11 HttpClient Bot")
                .header("Content-Type", "application/x-www-form-urlencoded")
                .build();

        try {
            HttpResponse<String> response = getHttpClient().send(request, HttpResponse.BodyHandlers.ofString());
            return response.body();
        } catch (IOException | InterruptedException e) {
            return e.toString();
        }
    }
}
