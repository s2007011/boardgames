package boardgames.frontend.requests;

import java.net.URI;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.util.Map;

public class DeleteRequest extends Request {

    public String send(String url, Map<Object, Object> parameters) {

        url += "?" + getStringFromMap(parameters);

        HttpRequest request = HttpRequest.newBuilder()
                .DELETE()
                .uri(URI.create(url))
                .setHeader("Front-End", "")
                .build();

        try {
            HttpResponse<String> response = getHttpClient().send(request, HttpResponse.BodyHandlers.ofString());
            return response.body();
        } catch (Exception e) {
            return e.toString();
        }
    }
}
