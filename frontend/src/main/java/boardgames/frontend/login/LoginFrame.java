package boardgames.frontend.login;

import boardgames.frontend.GlobalStaticVariables;
import boardgames.frontend.UserDetails;
import boardgames.frontend.main.MainFrame;
import boardgames.frontend.requests.PostRequest;

import javax.imageio.ImageIO;
import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static boardgames.frontend.login.ComponentConfigurations.*;

public class LoginFrame extends JFrame {

    private class Listener implements ActionListener, KeyListener {

        void checkLogin() {
            String userText = userTextField.getText();
            String pwdText = new String(passwordField.getPassword());

            Map<Object, Object> parameters = new HashMap<>();
            parameters.put("username", userText);
            parameters.put("password", pwdText);

            int response = -2;
            try {
                response = Integer.parseInt(new PostRequest().send("http://localhost/users/validate", parameters));
            } catch (Exception ignored) {
            }

            if (response == -2) {

                JOptionPane.showMessageDialog(getParent(), "Cannot connect to server",
                        "Error", JOptionPane.ERROR_MESSAGE);
            } else if (response == -1) {

                JOptionPane.showMessageDialog(getParent(), "Invalid Username or Password",
                        "Error", JOptionPane.ERROR_MESSAGE);
            } else {

                UserDetails.getInstance().setId(response);

                UserDetails.fetchUserDetailsAsynchronously();

                removeKeyListener(listener);
                dispose();
                new MainFrame();
            }
        }

        @Override
        public void actionPerformed(ActionEvent e) {

            Object source = e.getSource();

            if (source == loginButton) {

                checkLogin();

            } else if (source == registerButton) {

                dispose();
                new RegisterFrame();

            } else if (source == showPassword) {

                if (showPassword.isSelected()) {
                    passwordField.setEchoChar((char) 0);
                } else {
                    passwordField.setEchoChar('*');
                }
            }
        }

        @Override
        public void keyPressed(KeyEvent e) {
            if (e.getKeyCode() == KeyEvent.VK_ENTER) {

                if (getFocusOwner() == userTextField) {
                    passwordField.requestFocus();
                } else if (getFocusOwner() == passwordField || getFocusOwner() == loginButton) {
                    checkLogin();
                }
            }
        }

        @Override
        public void keyTyped(KeyEvent e) {
        }

        @Override
        public void keyReleased(KeyEvent e) {
        }
    }

    private static class ImagePanel extends JPanel {
        private BufferedImage image;

        public ImagePanel() {
            setBorder(new EmptyBorder(20, 0, 140, 0));
            try {
                image = ImageIO.read(new File("images/logo.png"));
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }

        @Override
        protected void paintComponent(Graphics g) {
            super.paintComponent(g);
            g.drawImage(image, 6, 0, 248, 188, this);
        }
    }

    private final Listener listener = new Listener();
    private final JTextField userTextField = new JTextField();
    private final JPasswordField passwordField = new JPasswordField();
    private final JButton loginButton = new JButton("LOGIN");
    private final JButton registerButton = new JButton("JOIN NOW");
    private final JCheckBox showPassword = new JCheckBox("Show Password");

    LoginFrame() {

        new GlobalStaticVariables();

        setTitle("Login Form");
        getRootPane().setBorder(new EmptyBorder(30, 20, 0, 20));

        addElements();
        addEvents();

        finaliseFrame(this, 480);
    }

    private void addElements() {

        JPanel imagePanel = new ImagePanel();
        imagePanel.setAlignmentX(0);

        JPanel buttonsPanel = createButtonsPanel(loginButton, registerButton);

        List<JComponent> components = Arrays.asList(
                imagePanel,
                new JLabel("USERNAME"),
                userTextField,
                new JLabel("PASSWORD"),
                passwordField,
                showPassword,
                buttonsPanel
        );

        configureComponents(components, 10);

        components.forEach(this::add);
    }

    public void addEvents() {
        showPassword.addActionListener(listener);
        loginButton.addActionListener(listener);
        registerButton.addActionListener(listener);

        userTextField.addKeyListener(listener);
        passwordField.addKeyListener(listener);
        loginButton.addKeyListener(listener);
    }

    public static void main(String[] args) {
        new PostRequest().send("http://localhost/recommended/update",null);
        new LoginFrame();
    }
}