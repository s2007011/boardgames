package boardgames.frontend.login;

import boardgames.frontend.layouts.WrapLayout;

import javax.swing.*;
import javax.swing.border.Border;
import javax.swing.border.EmptyBorder;
import java.awt.*;
import java.util.List;

public class ComponentConfigurations {

    public static void configureComponents(List<JComponent> components, int topLabelPadding) {

        final Border labelBorder = new EmptyBorder(topLabelPadding, 2, 0, 0);
        final Dimension textFieldSize = new Dimension(1000, 36);
        final Dimension buttonSize = new Dimension(125, 30);

        for (JComponent component : components) {

            if (component instanceof JLabel) {

                component.setBorder(labelBorder);
            } else if (component instanceof JTextField) {

                component.setPreferredSize(textFieldSize);
                component.setMaximumSize(textFieldSize);
                component.setAlignmentX(0);
            } else if (component instanceof JPanel) {

                component.setAlignmentX(0);

                for (Component button : component.getComponents()) {

                    button.setPreferredSize(buttonSize);
                }
            } else if (component instanceof JCheckBox) {

                component.setBorder(new EmptyBorder(0, 1, 20, 0));
            }
        }
    }

    static JPanel createButtonsPanel(JButton leftButton, JButton rightButton) {

        JPanel buttonsPanel = new JPanel(new WrapLayout(FlowLayout.CENTER));

        buttonsPanel.add(leftButton);
        buttonsPanel.add(rightButton);
        buttonsPanel.setAlignmentX(0);

        return buttonsPanel;
    }

    static void finaliseFrame(JFrame frame, int height) {

        final int width = 300;

        Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();

        frame.setLayout(new BoxLayout(frame.getContentPane(), BoxLayout.Y_AXIS));
        frame.setBounds((screenSize.width - width) / 2, (screenSize.height - height) / 2, width, height);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setResizable(false);
        frame.setVisible(true);
    }
}
