package boardgames.frontend.login;

import boardgames.frontend.entities.City;
import boardgames.frontend.requests.GetRequest;
import boardgames.frontend.requests.PostRequest;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static boardgames.frontend.login.ComponentConfigurations.*;

public class RegisterFrame extends JFrame {

    private class Listener implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent event) {

            Object source = event.getSource();

            if (source == backButton) {
                dispose();
                new LoginFrame();
            } else if (source == showPasswords) {

                if (showPasswords.isSelected()) {
                    passwordField.setEchoChar((char) 0);
                    repeatPasswordField.setEchoChar((char) 0);
                } else {
                    passwordField.setEchoChar('*');
                    repeatPasswordField.setEchoChar('*');
                }
            } else if (source == registerButton) {

                if (userTextField.getText().isEmpty() || new String(passwordField.getPassword()).isEmpty()) {

                    JOptionPane.showMessageDialog(getParent(), "Username and password cannot be null",
                            "Error", JOptionPane.ERROR_MESSAGE);

                    return;
                }

                if (!new String(passwordField.getPassword()).equals(new String(repeatPasswordField.getPassword()))) {

                    JOptionPane.showMessageDialog(getParent(), "Passwords do not match",
                            "Error", JOptionPane.ERROR_MESSAGE);

                    return;
                }

                Map<Object, Object> parameters = new HashMap<>();

                if (!ageTextField.getText().isEmpty()) try {

                    int age = Integer.parseInt(ageTextField.getText());
                    parameters.put("age", age);

                } catch (NumberFormatException e) {

                    JOptionPane.showMessageDialog(getParent(), "Age must be an integer",
                            "Error", JOptionPane.ERROR_MESSAGE);

                    return;
                }


                if (!surnameTextField.getText().isEmpty()) parameters.put("surname", surnameTextField.getText());
                if (!emailTextField.getText().isEmpty()) parameters.put("email", emailTextField.getText());
                if (!phoneTextField.getText().isEmpty()) parameters.put("phone", phoneTextField.getText());
                if (!nameTextField.getText().isEmpty()) parameters.put("name", nameTextField.getText());

                parameters.put("username", userTextField.getText());
                parameters.put("password", new String(passwordField.getPassword()));
                parameters.put("cityId", City.getIdFromString(cityList, String.valueOf(comboBox.getSelectedItem())));


                String response = new PostRequest().send("http://localhost/users/add", parameters);

                if (response.equals("Saved")) {
                    JOptionPane.showMessageDialog(getParent(), "User created successfully!",
                            "Error", JOptionPane.PLAIN_MESSAGE);

                    dispose();
                    new LoginFrame();

                } else if (response.equals("User exists")) {
                    JOptionPane.showMessageDialog(getParent(), "User exists!",
                            "Error", JOptionPane.ERROR_MESSAGE);
                } else {
                    JOptionPane.showMessageDialog(getParent(), "Cannot connect to server",
                            "Error", JOptionPane.ERROR_MESSAGE);
                }
            }
        }
    }

    private final Listener listener = new Listener();
    private final JTextField nameTextField = new JTextField();
    private final JTextField surnameTextField = new JTextField();
    private final JTextField emailTextField = new JTextField();
    private final JTextField phoneTextField = new JTextField();
    private final JTextField ageTextField = new JTextField();
    private final JTextField userTextField = new JTextField();
    private final JPasswordField passwordField = new JPasswordField();
    private final JPasswordField repeatPasswordField = new JPasswordField();
    private final JButton backButton = new JButton("BACK");
    private final JButton registerButton = new JButton("REGISTER");
    private final JCheckBox showPasswords = new JCheckBox("Show Passwords");

    private final ObjectMapper mapper = new ObjectMapper();
    private List<City> cityList;
    private JComboBox<String> comboBox;

    RegisterFrame() {

        setTitle("Register Form");
        getRootPane().setBorder(new EmptyBorder(10, 20, 0, 20));

        addElements();
        addEvents();

        finaliseFrame(this, 590);
    }

    private void addElements() {

        JPanel buttonsPanel = createButtonsPanel(backButton, registerButton);


        String json = new GetRequest().send("http://localhost/cities/all", null);

        String[] cities = new String[0];
        try {
            cityList = mapper.readValue(json, new TypeReference<>() {
            });

            cities = new String[cityList.size()];

            for (int i = 0; i < cityList.size(); i++) {

                cities[i] = cityList.get(i).toString();
            }
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }

        comboBox = new JComboBox<>(cities);
        comboBox.setAlignmentX(0);
        comboBox.setMaximumSize(new Dimension(300, comboBox.getPreferredSize().height));

        List<JComponent> components = Arrays.asList(
                new JLabel("NAME"),
                nameTextField,
                new JLabel("SURNAME"),
                surnameTextField,
                new JLabel("EMAIL"),
                emailTextField,
                new JLabel("PHONE"),
                phoneTextField,
                new JLabel("AGE"),
                ageTextField,
                new JLabel("LOCATION*"),
                comboBox,
                new JLabel("USERNAME*"),
                userTextField,
                new JLabel("PASSWORD*"),
                passwordField,
                new JLabel("PASSWORD REPEAT*"),
                repeatPasswordField,
                showPasswords,
                buttonsPanel
        );

        configureComponents(components, 0);

        components.forEach(this::add);
    }

    public void addEvents() {
        showPasswords.addActionListener(listener);
        backButton.addActionListener(listener);
        registerButton.addActionListener(listener);
    }
}