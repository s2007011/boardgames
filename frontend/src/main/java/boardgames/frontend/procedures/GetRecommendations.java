package boardgames.frontend.procedures;

import boardgames.frontend.procedures.entities.ExternalAndUserRatings;
import boardgames.frontend.requests.PostRequest;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class GetRecommendations {

    public static Map<Integer,Double> getProcedureResults(int uid) {
        Map<Object, Object> params = new HashMap<>();
        params.put("uId", uid);
        JSONParser parser = new JSONParser();
        JSONArray json= null;
        Map<Integer,Double> recommendations=new HashMap<Integer, Double>();
        try {
            json = (JSONArray) parser.
                    parse(new PostRequest().send("http://localhost/recommended/recommendations",params));
        for (Object o : json) {
            JSONObject single=(JSONObject)parser.parse(o.toString());
            for (Object o1 : single.keySet()) {
                recommendations.put(Integer.parseInt((String) o1),(Double)single.get(o1));
            }
        }
        JSONObject test=(JSONObject)parser.parse(json.get(0).toString());
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return recommendations;
    }

}
