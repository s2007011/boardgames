package boardgames.frontend.procedures;

import boardgames.frontend.requests.PostRequest;

import java.util.HashMap;
import java.util.Map;

public class LikeForReviewCalls {

    public static int userLikesReview(int uID,int rID,int eID){

        Map<Object, Object> params = new HashMap<>();
        params.put("userId", uID);
        params.put("reviewerUserId", rID);
        params.put("reviewerExpansionId",eID);
        String answer = new PostRequest().send("http://localhost/reviews/isLiked", params);
        return Integer.parseInt(answer);
    }

    public static void addLike(Integer uId,
                               Integer revieweruId,
                               Integer eId,
                               Integer rating){
        Map<Object, Object> params = new HashMap<>();
        params.put("uId", uId);
        params.put("revieweruId", revieweruId);
        params.put("eId", eId);
        params.put("rating",rating);
        new PostRequest().send("http://localhost/reviews/addLike", params);
    }

    public static void addReview( Integer uId,
                                  Integer eId,
                                 Integer rating,
                                 String comment){
        Map<Object, Object> params = new HashMap<>();
        params.put("uId", uId);
        params.put("eId", eId);
        params.put("rating",rating);
        params.put("comment",comment);
        new PostRequest().send("http://localhost/reviews/add", params);
    }

}
