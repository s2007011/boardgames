package boardgames.frontend.procedures.entities;

public class ExternalAndUserRatings {

    private String name;
    private int external_reviewer_id;
    private  float rating;
    private String review_description;
    private int weight;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getExternal_reviewer_id() {
        return external_reviewer_id;
    }

    public void setExternal_reviewer_id(int external_reviewer_id) {
        this.external_reviewer_id = external_reviewer_id;
    }

    public float getRating() {
        return rating;
    }

    public void setRating(float rating) {
        this.rating = rating;
    }

    public String getReview_description() {
        return review_description;
    }

    public void setReview_description(String review_description) {
        this.review_description = review_description;
    }

    public int getWeight() {
        return weight;
    }

    public void setWeight(int weight) {
        this.weight = weight;
    }
}
