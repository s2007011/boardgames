package boardgames.frontend.procedures.entities;

public class TopNUserReviews{
    private int user_id;
    private int rating;
    private String username;
    private String review_description;
    private float likeavg;

    public int getUser_id() {
        return user_id;
    }

    public void setUser_id(int user_id) {
        this.user_id = user_id;
    }

    public int getRating() {
        return rating;
    }

    public void setRating(int rating) {
        this.rating = rating;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getReview_description() {
        return review_description;
    }

    public void setReview_description(String review_description) {
        this.review_description = review_description;
    }

    public float getLikeavg() {
        return likeavg;
    }

    public void setLikeavg(float likeavg) {
        this.likeavg = likeavg;
    }
}