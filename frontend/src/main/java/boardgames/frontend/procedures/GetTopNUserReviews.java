package boardgames.frontend.procedures;

import boardgames.frontend.procedures.entities.TopNUserReviews;
import boardgames.frontend.requests.PostRequest;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static boardgames.frontend.GlobalStaticVariables.getObjectMapper;

public class GetTopNUserReviews {

    private GetTopNUserReviews() {
    }

    public static List<TopNUserReviews> getProcedureResults(int eid, int topN) throws JsonProcessingException {

        Map<Object, Object> params = new HashMap<>();
        params.put("eId", eid);
        params.put("topN", topN);

        String json = new PostRequest().send("http://localhost/reviews/topNReviews", params);
        json = json.substring(1, json.length() - 3);

        List<TopNUserReviews> topReviews = getObjectMapper().readValue(json, new TypeReference<>() {
        });

        return topReviews;
    }
}
