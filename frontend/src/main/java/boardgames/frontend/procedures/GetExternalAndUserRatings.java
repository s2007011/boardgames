package boardgames.frontend.procedures;

import boardgames.frontend.procedures.entities.ExternalAndUserRatings;
import boardgames.frontend.requests.PostRequest;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static boardgames.frontend.GlobalStaticVariables.getObjectMapper;

/**
 * Use only statically. GetExternalAndUserRatings.GetProcedureResults
 */
public class GetExternalAndUserRatings {

    private GetExternalAndUserRatings() {
    }

    public static List<ExternalAndUserRatings> getProcedureResults(int eid, int uid) {

        Map<Object, Object> params = new HashMap<>();
        params.put("eId", eid);
        params.put("uId", uid);

        String json = new PostRequest().send("http://localhost/reviews/GetExternalAndUserRatings", params);
        json = json.substring(1, json.length() - 3);


        try {
            List<ExternalAndUserRatings> externalAndUserRatings = getObjectMapper().readValue(json, new TypeReference<>() {
            });
            externalAndUserRatings.forEach(System.out::print);
            return externalAndUserRatings;
        } catch (JsonProcessingException e) {
            e.printStackTrace();
            return null;
        }
    }
}
