package boardgames.frontend.settings;

import boardgames.frontend.UserDetails;
import boardgames.frontend.entities.ExternalReviewerWeight;
import boardgames.frontend.requests.PostRequest;

import javax.swing.*;
import javax.swing.border.Border;
import javax.swing.border.EmptyBorder;
import javax.swing.border.LineBorder;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;
import java.util.Map;
import java.util.stream.IntStream;

import static boardgames.frontend.GlobalStaticVariables.getServerUrl;
import static java.util.stream.Collectors.toList;

public class ReviewerPanel extends JPanel {

    private static class Listener implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent e) {

            JRadioButton radioButton = (JRadioButton) e.getSource();

            ReviewerPanel reviewerPanel = (ReviewerPanel) radioButton.getParent();

            if (reviewerPanel.getExternalReviewerWeight() != null) {
                new PostRequest().send(getServerUrl() + "/reviewers/edit", Map.of(
                        "userId", reviewerPanel.getExternalReviewerWeight().getUserId(),
                        "externalReviewerId", reviewerPanel.getExternalReviewerWeight().getExternalReviewerId(),
                        "weight", reviewerPanel.getButtonGroup().getSelection().getActionCommand()));
            } else {
                new PostRequest().send(getServerUrl() + "users/changeUserReviewWeight", Map.of(
                        "userId", UserDetails.getUser().getId(),
                        "userReviewWeight", reviewerPanel.getButtonGroup().getSelection().getActionCommand()
                ));
            }
        }
    }

    private static final Border border = new LineBorder(Color.gray, 1, true);
    private static final Listener listener = new Listener();

    private ButtonGroup buttonGroup;

    private List<JRadioButton> radioButtons;

    private ExternalReviewerWeight externalReviewerWeight;

    ReviewerPanel(ExternalReviewerWeight externalReviewerWeight) {

        this.externalReviewerWeight = externalReviewerWeight;

        setPreferredSize(new Dimension(200, 300));
        setBorder(border);
        setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));

        JLabel name = new JLabel(externalReviewerWeight.getExternalReviewer().getName());
        name.setAlignmentX(CENTER_ALIGNMENT);
        name.setBorder(new EmptyBorder(30, 0, 30, 0));
        add(name);

        radioButtons = IntStream.rangeClosed(1, 5)
                .mapToObj(i -> new JRadioButton(Integer.toString(i)))
                .collect(toList());

        radioButtons.get(externalReviewerWeight.getWeight() - 1).setSelected(true);

        buttonGroup = new ButtonGroup();

        for (JRadioButton radioButton : radioButtons) {

            radioButton.setActionCommand(radioButton.getText());
            radioButton.setBorder(new EmptyBorder(10, 0, 10, 0));
            radioButton.setAlignmentX(CENTER_ALIGNMENT);
            buttonGroup.add(radioButton);
            add(radioButton);
            radioButton.addActionListener(listener);
        }
    }

    ReviewerPanel() {

        setPreferredSize(new Dimension(200, 300));
        setBorder(border);
        setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));

        JLabel name = new JLabel("Reviews from other users");
        name.setAlignmentX(CENTER_ALIGNMENT);
        name.setBorder(new EmptyBorder(30, 0, 30, 0));
        add(name);

        radioButtons = IntStream.rangeClosed(1, 5)
                .mapToObj(i -> new JRadioButton(Integer.toString(i)))
                .collect(toList());

        radioButtons.get(UserDetails.getUser().getUserReviewWeight() - 1).setSelected(true);

        buttonGroup = new ButtonGroup();

        for (JRadioButton radioButton : radioButtons) {

            radioButton.setActionCommand(radioButton.getText());
            radioButton.setBorder(new EmptyBorder(10, 0, 10, 0));
            radioButton.setAlignmentX(CENTER_ALIGNMENT);
            buttonGroup.add(radioButton);
            add(radioButton);
            radioButton.addActionListener(listener);
        }
    }

    public ButtonGroup getButtonGroup() {
        return buttonGroup;
    }

    public ExternalReviewerWeight getExternalReviewerWeight() {
        return externalReviewerWeight;
    }
}
