package boardgames.frontend.settings;

import boardgames.frontend.UserDetails;
import boardgames.frontend.entities.User;
import boardgames.frontend.requests.PostRequest;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static boardgames.frontend.login.ComponentConfigurations.configureComponents;

public class SettingsSidePanel extends JPanel {

    static final int padding = 20;

    SettingsSidePanel() {

        setBorder(new EmptyBorder(padding / 2, padding, 0, padding));
        setPreferredSize(new Dimension(300, 600));
        setBackground(Color.GRAY);
        setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));


        if (UserDetails.getUser() == null) synchronized (UserDetails.getInstance()) {
            try {
                UserDetails.getInstance().wait();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        User user = UserDetails.getUser();

        final JTextField nameTextField = new JTextField(user.getName());
        final JTextField surnameTextField = new JTextField(user.getSurname());
        final JTextField emailTextField = new JTextField(user.getEmail());
        final JTextField phoneTextField = new JTextField(user.getPhone());
        final JTextField ageTextField = new JTextField(user.getAge() == null ? "" : user.getAge().toString());
        final JPasswordField passwordField = new JPasswordField();
        final JPasswordField repeatPasswordField = new JPasswordField();
        final JCheckBox showPasswords = new JCheckBox("Show Passwords");
        final JButton saveChanges = new JButton("SAVE CHANGES");

        saveChanges.addActionListener(e -> JOptionPane.showMessageDialog(this, "Changes saved"));


        List<JComponent> components = Arrays.asList(
                new JLabel("Username: " + user.getUsername()),
                new JLabel("NAME"),
                nameTextField,
                new JLabel("SURNAME"),
                surnameTextField,
                new JLabel("EMAIL"),
                emailTextField,
                new JLabel("PHONE"),
                phoneTextField,
                new JLabel("AGE"),
                ageTextField,
                new JLabel("CHANGE PASSWORD"),
                passwordField,
                new JLabel("PASSWORD REPEAT"),
                repeatPasswordField,
                showPasswords,
                saveChanges
        );

        configureComponents(components, 16);

        components.forEach(this::add);
    }
}
