package boardgames.frontend.settings;

import boardgames.frontend.UserDetails;
import boardgames.frontend.entities.ExternalReviewer;
import boardgames.frontend.entities.ExternalReviewerWeight;
import boardgames.frontend.layouts.WrapLayout;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;
import java.util.List;
import java.util.stream.IntStream;

import static java.util.stream.Collectors.toList;

class SettingsMainPanel extends JPanel {

    private JScrollPane mainScrollPane;

    SettingsMainPanel() {

        int mainPanelPadding = 18;
        setLayout(new WrapLayout(WrapLayout.LEFT, mainPanelPadding, mainPanelPadding));
        setMinimumSize(new Dimension(880, 600));
        setMaximumSize(new Dimension(880, Integer.MAX_VALUE));
        setBackground(Color.white);


        new Thread(() -> {


            if (UserDetails.getExternalReviewerWeights() == null) synchronized (UserDetails.getInstance()) {
                try {
                    UserDetails.getInstance().wait();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }

            ReviewerPanel userReviewerPanel = new ReviewerPanel();

            add(userReviewerPanel);

            List<ReviewerPanel> reviewerPanels = UserDetails.getExternalReviewerWeights()
                    .stream()
                    .map(ReviewerPanel::new)
                    .collect(toList());

            reviewerPanels.forEach(this::add);

            revalidate();

        }).start();

        configureMainScrollPane();
    }

    private void configureMainScrollPane() {

        mainScrollPane = new JScrollPane(this);
        mainScrollPane.setBorder(new EmptyBorder(0, 0, 0, 0));
        mainScrollPane.setPreferredSize(new Dimension(900, 600));
        mainScrollPane.getVerticalScrollBar().setUnitIncrement(16);
    }

    public JScrollPane getScrollPane() {
        return mainScrollPane;
    }
}
