package boardgames.frontend.settings;

import javax.swing.*;
import java.awt.*;

public class SettingsPanel extends JPanel {

    public SettingsPanel() {
        setName("SETTINGS");
        setLayout(new BorderLayout());

        add(new SettingsSidePanel(), BorderLayout.WEST);
        add(new SettingsMainPanel().getScrollPane(), BorderLayout.EAST);
    }
}
