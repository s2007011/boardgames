package boardgames.frontend.home;

import javax.swing.*;
import java.awt.*;

public class HomePanel extends JPanel {

    public HomePanel() {
        setName("HOME");
        setLayout(new BorderLayout());

        add(new HomeSidePanel(), BorderLayout.WEST);
        add(new HomeMainPanel().getScrollPane(), BorderLayout.EAST);
    }
}
