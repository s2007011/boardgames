package boardgames.frontend.home;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;

public class HomeSidePanel extends JPanel {

    int padding = 20;

    HomeSidePanel() {

        setBorder(new EmptyBorder(padding / 2, padding, padding / 2, padding));
        setPreferredSize(new Dimension(300, 600));
        setBackground(Color.GRAY);
        setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));

        JLabel recommendationsLabel = new JLabel("RECOMMENDATIONS");
        recommendationsLabel.setFont(new Font("a font", Font.BOLD, 20));
        recommendationsLabel.setForeground(Color.white);

        recommendationsLabel.setAlignmentY(CENTER_ALIGNMENT);
        recommendationsLabel.setAlignmentX(CENTER_ALIGNMENT);

        add(recommendationsLabel);
    }
}
