package boardgames.frontend.home;


import boardgames.frontend.UserDetails;
import boardgames.frontend.games.GamePanel;
import boardgames.frontend.layouts.WrapLayout;
import boardgames.frontend.procedures.GetRecommendations;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;
import java.util.Map;

import static boardgames.frontend.GlobalStaticVariables.*;

public class HomeMainPanel extends JPanel {

    private JScrollPane mainScrollPane;

    HomeMainPanel() {

        int mainPanelPadding = 18;
        setLayout(new WrapLayout(WrapLayout.LEFT, mainPanelPadding, mainPanelPadding));
        setMinimumSize(new Dimension(880, 600));
        setMaximumSize(new Dimension(880, Integer.MAX_VALUE));
        setBackground(Color.white);

        try {

            if (getGamePanels() == null) synchronized (getServerUrl()) {
                getServerUrl().wait();
            }

            Map<Integer, Double> recommendations = GetRecommendations.getProcedureResults(UserDetails.getUser().getId());

            getGames().stream()
                    .filter(game -> recommendations.containsKey(game.getId()))
                    .map(game -> new GamePanel(game, false))
                    .forEach(this::add);

        } catch (InterruptedException e) {
            e.printStackTrace();
            System.exit(-1);
        }

        repaint();
        revalidate();

        configureMainScrollPane();
    }

    private void configureMainScrollPane() {

        mainScrollPane = new JScrollPane(this);
        mainScrollPane.setBorder(new EmptyBorder(0, 0, 0, 0));
        mainScrollPane.setPreferredSize(new Dimension(900, 600));
        mainScrollPane.getVerticalScrollBar().setUnitIncrement(16);
    }

    public JScrollPane getScrollPane() {
        return mainScrollPane;
    }
}
