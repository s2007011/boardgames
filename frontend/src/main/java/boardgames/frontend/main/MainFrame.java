package boardgames.frontend.main;

import boardgames.frontend.community.CommunityPanel;
import boardgames.frontend.games.GamesScrollPanel;
import boardgames.frontend.home.HomePanel;
import boardgames.frontend.settings.SettingsPanel;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.Arrays;
import java.util.List;

public class MainFrame extends JFrame {

    private class Listener implements MouseListener {

        @Override
        public void mouseClicked(MouseEvent e) {
        }

        @Override
        public void mousePressed(MouseEvent e) {
            Object source = e.getSource();
            if (menuLabelsList.contains(source)) {
                JLabel label = (JLabel) source;
                cardLayout.show(cardPanel, label.getText());

                selectedLabel.setForeground(Color.GRAY);
                selectedLabel = label;
                selectedLabel.setForeground(Color.WHITE);
            }
        }

        @Override
        public void mouseReleased(MouseEvent e) {
        }

        @Override
        public void mouseEntered(MouseEvent e) {

            Object o = e.getSource();

            if (menuLabelsList.contains(o) && !selectedLabel.equals(o)) {
                JLabel label = (JLabel) o;

                label.setForeground(Color.LIGHT_GRAY);
            }
        }

        @Override
        public void mouseExited(MouseEvent e) {

            Object o = e.getSource();

            if (menuLabelsList.contains(o) && !selectedLabel.equals(o)) {
                JLabel label = (JLabel) o;

                label.setForeground(Color.GRAY);
            }
        }
    }

    final int width = 1200;
    final int height = 700;

    final Font menuFont = new Font("menu font", Font.BOLD, 30);
    final Listener listener = new Listener();
    final JLabel homeLabel = new JLabel("HOME");
    final JLabel gamesLabel = new JLabel("GAMES");
    final JLabel communityLabel = new JLabel("COMMUNITY");
    final JLabel settingsLabel = new JLabel("SETTINGS");
    final List<JLabel> menuLabelsList = Arrays.asList(homeLabel, gamesLabel, communityLabel, settingsLabel);

    JLabel selectedLabel = homeLabel;

    final CardLayout cardLayout = new CardLayout();
    final JPanel menuPanel = new JPanel(new FlowLayout(FlowLayout.LEFT));
    final JPanel cardPanel = new JPanel(cardLayout);
    final JPanel homePanel = new HomePanel();
    final JPanel gamesPanel = new GamesScrollPanel();
    final JPanel communityPanel = new CommunityPanel();
    final JPanel settingsPanel = new SettingsPanel();
    final List<JPanel> mainPanelsList = Arrays.asList(homePanel, gamesPanel, communityPanel, settingsPanel);


    public MainFrame() {
        setTitle("Board Games");

        Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();

        setBounds((screenSize.width - width) / 2, (screenSize.height - height) / 2, width, height);
        setResizable(false);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setLayout(new BorderLayout());

        menuPanel.setBackground(Color.DARK_GRAY);
        menuPanel.setPreferredSize(new Dimension(width, 60));
        add(menuPanel, BorderLayout.NORTH);

        for (JLabel menuLabel : menuLabelsList) {
            menuLabel.setBorder(new EmptyBorder(7, 30, 0, 0));
            menuLabel.setForeground(Color.GRAY);
            menuLabel.addMouseListener(listener);
            menuLabel.setFont(menuFont);
            menuPanel.add(menuLabel);
        }
        selectedLabel.setForeground(Color.WHITE);

        cardPanel.setPreferredSize(new Dimension(width, 640));

        mainPanelsList.forEach(panel -> {
            panel.setBounds(0, 60, width, 640);
            cardPanel.add(panel.getName(), panel);
        });

        add(cardPanel, BorderLayout.CENTER);

        setVisible(true);
    }

    public static void main(String[] args) {
        new MainFrame();
    }
}
