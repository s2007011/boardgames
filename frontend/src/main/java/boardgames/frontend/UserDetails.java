package boardgames.frontend;

import boardgames.frontend.entities.Expansion;
import boardgames.frontend.entities.ExternalReviewerWeight;
import boardgames.frontend.entities.Game;
import boardgames.frontend.entities.User;
import boardgames.frontend.requests.PostRequest;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.util.*;

import static boardgames.frontend.GlobalStaticVariables.*;
import static java.util.stream.Collectors.toList;

public class UserDetails {
    private static class Singleton {
        private static final UserDetails INSTANCE = new UserDetails();
    }

    private static int id;
    private static User user;
    private static List<Integer> ownedExpansionIds;

    private UserDetails() {
    }

    public static List<Integer> getOwnedExpansionIds() {
        return ownedExpansionIds;
    }

    private static List<ExternalReviewerWeight> externalReviewerWeights;

    public static UserDetails getInstance() {
        return Singleton.INSTANCE;
    }

    public static List<ExternalReviewerWeight> getExternalReviewerWeights() {
        return externalReviewerWeights;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        UserDetails.id = id;
    }

    public static void fetchUserDetailsAsynchronously() {
        new Thread(UserDetails::fetchUserDetails).start();
    }

    public static User getUser() {
        return user;
    }

    public static List<Integer> getOwnedGameIds() {

        List<Integer> ownedGameIds = new ArrayList<>();

        for (Game game : getGames()) {
            for (Expansion expansion : game.getExpansions()) {
                for (Integer ownedExpansionId : ownedExpansionIds) {
                    if (expansion.getId() == ownedExpansionId && !ownedGameIds.contains(game.getId())) {
                        ownedGameIds.add(game.getId());
                    }
                }
            }
        }

        return ownedGameIds;
    }

    public static void fetchUserDetails() {

        try {
            ObjectMapper mapper = getObjectMapper();

            String json = new PostRequest().send(getServerUrl() + "users/expansionIds", Map.of("userId", id));

            ownedExpansionIds = getObjectMapper().readValue(json, new TypeReference<>() {
            });

            json = new PostRequest().send(getServerUrl() + "users/externalReviewersWeights",
                    Map.of("userId", id));

            externalReviewerWeights = getObjectMapper().readValue(json, new TypeReference<>() {
            });

            json = new PostRequest().send(getServerUrl() + "users/get", Map.of("userId", id));

            user = mapper.readValue(json, new TypeReference<>() {
            });

            List<Game> games = getGames();

            if (games == null) synchronized (getServerUrl()) {
                getServerUrl().wait();
            }

            synchronized (getInstance()) {
                getInstance().notifyAll();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}