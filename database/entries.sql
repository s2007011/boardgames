use s2007066_project;

INSERT INTO city VALUES (DEFAULT, 'Edinburgh', 'UK');
INSERT INTO city VALUES (DEFAULT, 'Glasgow', 'UK');
INSERT INTO city VALUES (DEFAULT, 'Aberdeen', 'UK');
INSERT INTO city VALUES (DEFAULT, 'Dundee', 'UK');

INSERT INTO user VALUES (DEFAULT, 'user', '12345', 'John', 'Doe', 'john@email.com', '123456', '20', '3', '1');

insert into genre values (DEFAULT, 'Board Games');
insert into genre values (DEFAULT, 'Miniature War Games');
insert into genre values (DEFAULT, 'Card Games');
insert into genre values (DEFAULT, 'Role-Playing Games');


# board games
insert into game values (DEFAULT, 'Pandemic', 2, 4, 'https://cf.geekdo-images.com/itemrep/img/cTrAWasNHyKMcNs8Zrv5O7sKS6M=/fit-in/246x300/pic1534148.jpg');
insert into game values (DEFAULT, 'Wingspan', 1, 5, 'https://cf.geekdo-images.com/itemrep/img/vb971Kg92dzMd1TM3RBJtQm-XCU=/fit-in/246x300/pic4458123.jpg');
insert into game values (DEFAULT, 'Space Base', 2, 5, 'https://cf.geekdo-images.com/itemrep/img/s1H13XWVs8WFmsU0w4tflSjQbY0=/fit-in/246x300/pic4017302.jpg');

#assign genres
set @genre_id = (select id from genre where name = 'Board Games');
set @game_id = (select id from game where name = 'Pandemic');
insert into game_genre values (@game_id, @genre_id);
set @game_id = (select id from game where name = 'Wingspan');
insert into game_genre values (@game_id, @genre_id);
set @game_id = (select id from game where name = 'Space Base');
insert into game_genre values (@game_id, @genre_id);


# miniature war games
insert into game values (DEFAULT, 'Infinity', 2, 2, 'https://cf.geekdo-images.com/itemrep/img/4kQqcnnsZbphgS1fm__w0m_D-Y0=/fit-in/246x300/pic2351734.jpg');
insert into game values (DEFAULT, 'Warhammer Age of Sigmar', 2, 2, 'https://cf.geekdo-images.com/itemrep/img/GoVM7TV5gmf98cMxsBdnJWAxaz8=/fit-in/246x300/pic2615670.jpg');
insert into game values (DEFAULT, 'Malifaux', 2, 2, 'https://cf.geekdo-images.com/itemrep/img/zdzeJcucabZGfrDI2cgwKJZfpjs=/fit-in/246x300/pic1451000.jpg');


#assign genres
set @genre_id = (select id from genre where name = 'Miniature War Games');
set @game_id = (select id from game where name = 'Infinity');
insert into game_genre values (@game_id, @genre_id);
set @game_id = (select id from game where name = 'Warhammer Age of Sigmar');
insert into game_genre values (@game_id, @genre_id);
set @game_id = (select id from game where name = 'Malifaux');
insert into game_genre values (@game_id, @genre_id);

# card games
insert into game values (DEFAULT, 'Magic: The Gathering', 2, 2, 'https://cf.geekdo-images.com/itemrep/img/p7rasc2QEuPGIEb9jQhQvyzimsg=/fit-in/246x300/pic163749.jpg');
insert into game values (DEFAULT, 'KeyForge: Call of the Archons', 2, 2, 'https://cf.geekdo-images.com/itemrep/img/XA_-_lo9ZwgGmg1ySjt7IhpKgDg=/fit-in/246x300/pic4298867.jpg');
insert into game values (DEFAULT, 'Pokémon Trading Card Game', 2, 2, 'https://cf.geekdo-images.com/itemrep/img/QpwheqfDlxt0-Pfz0r8hOTwxEvg=/fit-in/246x300/pic1807805.jpg');

#assign genres
set @genre_id = (select id from genre where name = 'Card Games');
set @game_id = (select id from game where name = 'Magic: The Gathering');
insert into game_genre values (@game_id, @genre_id);
set @game_id = (select id from game where name = 'KeyForge: Call of the Archons');
insert into game_genre values (@game_id, @genre_id);
set @game_id = (select id from game where name = 'Pokémon Trading Card Game');
insert into game_genre values (@game_id, @genre_id);


#role-playing games
insert into game values (DEFAULT, 'Shadowrun: Crossfire', 1, 4, 'https://cf.geekdo-images.com/itemrep/img/cZjP7LuRVpvrtY9MJJReV6rC2ZM=/fit-in/246x300/pic2060466.jpg');
insert into game values (DEFAULT, 'Mutant Chronicles', 2, 5, 'https://cf.geekdo-images.com/itemrep/img/I72E8mTwNeDYj7x38xFRVPoLGaY=/fit-in/246x300/pic314027.jpg');


#assign genres
set @genre_id = (select id from genre where name = 'Role-Playing Games');
set @game_id = (select id from game where name = 'Shadowrun: Crossfire');
insert into game_genre values (@game_id, @genre_id);
set @game_id = (select id from game where name = 'Mutant Chronicles: Siege of the Citadel');
insert into game_genre values (@game_id, @genre_id);


INSERT INTO expansion VALUES (DEFAULT, 'Classic', TRUE, 1);
INSERT INTO expansion VALUES (DEFAULT, 'Classic', TRUE, 2);
INSERT INTO expansion VALUES (DEFAULT, 'Classic', TRUE, 3);
INSERT INTO expansion VALUES (DEFAULT, 'Classic', TRUE, 4);
INSERT INTO expansion VALUES (DEFAULT, 'Classic', TRUE, 5);
INSERT INTO expansion VALUES (DEFAULT, 'Classic', TRUE, 6);
INSERT INTO expansion VALUES (DEFAULT, 'Classic', TRUE, 7);
INSERT INTO expansion VALUES (DEFAULT, 'Classic', TRUE, 8);
INSERT INTO expansion VALUES (DEFAULT, 'Classic', TRUE, 9);
INSERT INTO expansion VALUES (DEFAULT, 'Classic', TRUE, 10);
INSERT INTO expansion VALUES (DEFAULT, 'Classic', TRUE, 11);
INSERT INTO expansion VALUES (DEFAULT, 'On the Brink', TRUE, 1);

#Procedures


create procedure GetTopLikedExpansionReview(IN minimumNumberOfLikes int,IN  topN int)
begin
    select likes.reviewer_expansion_id,AVG(likes.`like`)
    from like_for_review as likes, user_expansion as owned
    where likes.reviewer_expansion_id=owned.expansion_id and
            (select COUNT(*)
             from like_for_review as like1
             where like1.reviewer_expansion_id=likes.reviewer_expansion_id)>minimumNumberOfLikes
    group by likes.reviewer_expansion_id
    ORDER BY AVG(likes.`like`) DESC LIMIT topN;
end;

drop procedure GetRatingsAndWeights;
create procedure GetRatingsAndWeights(IN eId int, In uId int)
begin
    select  name,external_reviewer_expansion.external_reviewer_id,rating,review_description,weight
    from external_reviewer_expansion,external_reviewer,external_reviewer_weight
    where expansion_id=eId and external_reviewer_expansion.external_reviewer_id=external_reviewer.id and
            external_reviewer_weight.user_id=uId and external_reviewer_weight.external_reviewer_id=external_reviewer.id
    union
    select 'users',0, avg(rating),'no description',user_review_weight
    from user_expansion,user
    where expansion_id=eId and user_id=uId;
end;


describe club;

INSERT INTO club values (DEFAULT, 'Geeks Club', 1);
INSERT INTO club values (DEFAULT, 'Board Games Club', 1);
INSERT INTO club values (DEFAULT, 'Miniature Games Club', 1);

select * from club;

INSERT INTO external_reviewer VALUES (DEFAULT, 'Amazon', 'www.amazon.com');
INSERT INTO external_reviewer VALUES (DEFAULT, 'Walmart', 'www.walmart.com');

