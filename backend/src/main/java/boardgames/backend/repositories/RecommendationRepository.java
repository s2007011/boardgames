package boardgames.backend.repositories;

import boardgames.backend.entities.Recommendation;
import boardgames.backend.entities.RecommendationPK;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RecommendationRepository extends JpaRepository<Recommendation, RecommendationPK> {
}
