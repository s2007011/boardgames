package boardgames.backend.repositories;

import boardgames.backend.entities.User;
import boardgames.backend.entities.UserExpansion;
import boardgames.backend.entities.UserExpansionPK;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface UserExpansionRepository extends JpaRepository<UserExpansion, UserExpansionPK> {
    List<UserExpansion> findByUserId(Integer user_id);
}
