package boardgames.backend.repositories;

import boardgames.backend.entities.GameGenre;
import boardgames.backend.entities.GameGenrePK;
import org.springframework.data.jpa.repository.JpaRepository;

public interface GameGenreRepository extends JpaRepository<GameGenre, GameGenrePK> {
}
