package boardgames.backend.repositories;

import boardgames.backend.entities.Expansion;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ExpansionRepository extends JpaRepository<Expansion, Integer> {
}
