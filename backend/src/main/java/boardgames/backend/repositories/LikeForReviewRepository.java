package boardgames.backend.repositories;

import boardgames.backend.entities.LikeForReview;
import boardgames.backend.entities.LikeForReviewPK;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface LikeForReviewRepository extends JpaRepository<LikeForReview, LikeForReviewPK> {

}
