package boardgames.backend.repositories;

import boardgames.backend.entities.Friend;
import boardgames.backend.entities.FriendPK;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface FriendRepository extends JpaRepository<Friend, FriendPK> {

    List<Integer> getFriendsByUserId1(int userId1);
}
