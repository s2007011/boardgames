package boardgames.backend.repositories;

import boardgames.backend.entities.ExternalReviewerWeight;
import boardgames.backend.entities.ExternalReviewerWeightPK;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface ExternalReviewerWeightRepository extends JpaRepository<ExternalReviewerWeight, ExternalReviewerWeightPK> {

    List<ExternalReviewerWeight> findAllByUserId(int userId);
}
