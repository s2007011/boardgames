package boardgames.backend.controllers;

import boardgames.backend.entities.City;
import boardgames.backend.repositories.CityRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(path = "/cities")
public class CityController {
    @Autowired
    private CityRepository cityRepository;

    @GetMapping(path = "/all")
    public Iterable<City> getCities() {
        return cityRepository.findAll();
    }
}
