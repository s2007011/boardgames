package boardgames.backend.controllers;

import boardgames.backend.entities.Friend;
import boardgames.backend.entities.FriendPK;
import boardgames.backend.repositories.FriendRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping(path = "/friends")
public class FriendController {

    @Autowired
    private FriendRepository friendRepository;

    @PostMapping(path = "friendIds")
    public List<Integer> getFriendIds(@RequestParam int userId) {
        return friendRepository.getFriendsByUserId1(userId);
    }

    @PostMapping(path = "/add")
    public String addFriend(@RequestParam int userId, @RequestParam int friendId) {

        Friend friend = new Friend();
        friend.setUserId1(userId);
        friend.setUserId1(friendId);

        friendRepository.save(friend);

        return "Success";
    }
}
