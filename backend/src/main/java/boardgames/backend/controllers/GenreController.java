package boardgames.backend.controllers;

import boardgames.backend.entities.GameGenre;
import boardgames.backend.entities.Genre;
import boardgames.backend.repositories.GameGenreRepository;
import boardgames.backend.repositories.GenreRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping(path = "/genres")
public class GenreController {

    @Autowired
    private GenreRepository genreRepository;

    @Autowired
    private GameGenreRepository gameGenreRepository;

    @GetMapping(path = "/all")
    public List<Genre> getAllGenres() {
        return genreRepository.findAll();
    }

    @GetMapping(path = "/ofGames")
    public List<GameGenre> getGameGenres() {
        return gameGenreRepository.findAll();
    }

}
