package boardgames.backend.controllers;

import boardgames.backend.entities.Expansion;
import boardgames.backend.entities.UserExpansion;
import boardgames.backend.repositories.ExpansionRepository;
import boardgames.backend.repositories.UserExpansionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(path = "/expansions")
public class ExpansionController {
    @Autowired
    private ExpansionRepository expansionRepository;

    @Autowired
    private UserExpansionRepository userExpansionRepository;

    @GetMapping(path = "/all")
    public Iterable<Expansion> getAllExpansions() {
        return expansionRepository.findAll();
    }

    @PostMapping
    public String addExpansion(@RequestParam int userId,
                               @RequestParam int expansionId,
                               @RequestParam(required = false) int rating,
                               @RequestParam(required = false) String reviewDescription,
                               @RequestParam(required = false) String houseRules) {

        UserExpansion userExpansion = new UserExpansion();

        userExpansion.setUserId(userId);
        userExpansion.setExpansionId(expansionId);
        userExpansion.setRating(rating);
        userExpansion.setReviewDescription(reviewDescription);
        userExpansion.setHouseRules(houseRules);

        userExpansionRepository.save(userExpansion);

        return "Success";
    }
}
