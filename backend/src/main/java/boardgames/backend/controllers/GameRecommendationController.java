package boardgames.backend.controllers;

import boardgames.backend.entities.Recommendation;
import boardgames.backend.entities.UserExpansion;
import boardgames.backend.repositories.RecommendationRepository;
import boardgames.backend.repositories.UserExpansionRepository;
import boardgames.backend.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.io.*;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;

import org.apache.mahout.cf.taste.common.TasteException;
import org.apache.mahout.cf.taste.impl.common.LongPrimitiveIterator;
import org.apache.mahout.cf.taste.impl.model.file.FileDataModel;
import org.apache.mahout.cf.taste.impl.recommender.GenericItemBasedRecommender;
import org.apache.mahout.cf.taste.impl.similarity.PearsonCorrelationSimilarity;
import org.apache.mahout.cf.taste.model.DataModel;
import org.apache.mahout.cf.taste.recommender.RecommendedItem;

@RestController
@RequestMapping(path = "/recommended")
public class GameRecommendationController {

    @Autowired
    private UserRepository users;

    @Autowired
    private UserExpansionRepository userExpansions;

    @Autowired
    private RecommendationRepository recommendationRepo;

    public static Date StringToDate(String dob) throws ParseException {
        //Instantiating the SimpleDateFormat class
        SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
        //Parsing the given String to Date object
        Date date = null;
        try {
            date = formatter.parse(dob);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        System.out.println("Date object value: "+date);
        return date;
    }

    /**
     * This function will update the game recommendations created using item-item collaborative filtering-with pearson
     * correlation similarity. Initially, chekcks whether the similarities have been calculated today. If yes, it
     * returns without updating anything.
     */
    @PostMapping(path = "/update")
    public void Update(){
        try {
            File dateFile=new File("date.txt");
            if(dateFile.exists()){
                BufferedReader reader=new BufferedReader(new FileReader("date.txt"));
                Date date = StringToDate(reader.readLine());
                final Calendar cal =Calendar.getInstance();
                cal.add(Calendar.DATE,-1);
                if(!date.before(cal.getTime())){
                    System.out.println("Is today");
                    return;
                }
                reader.close();
            }
            List<UserExpansion> ownedExpansions= userExpansions.findAll();
            BufferedWriter dateWriter=new BufferedWriter(new FileWriter("date.txt"));
            String pattern = "dd-MM-yyyy";
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
            String date = simpleDateFormat.format(Calendar.getInstance().getTime());
            dateWriter.write(date);
            dateWriter.flush();
            dateWriter.close();
            BufferedWriter writer=new BufferedWriter(new FileWriter("datamodel.csv"));
            for (UserExpansion ownedExpansion : ownedExpansions) {
                if(ownedExpansion.getRating()==null)
                    continue;
                writer.write(ownedExpansion.getUserId()+","+ownedExpansion.getExpansionId()+","+ownedExpansion.getRating()+"\n");
            }
            writer.flush();
            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ParseException e) {
            e.printStackTrace();
        }
        try {
            DataModel dm = new FileDataModel(new File("datamodel.csv"));
            //ItemSimilarity sim = new LogLikelihoodSimilarity(dm);
            PearsonCorrelationSimilarity sim = new PearsonCorrelationSimilarity(dm);
            GenericItemBasedRecommender recommender = new GenericItemBasedRecommender(dm, sim);

            int x=1;
            List<Recommendation> allRecomms=new ArrayList<>();
            for(LongPrimitiveIterator items = dm.getItemIDs(); items.hasNext();) {
                long itemId = items.nextLong();
                List<RecommendedItem>recommendations = recommender.mostSimilarItems(itemId, 4);
                for(RecommendedItem recommendation : recommendations) {

                    Recommendation recom=new Recommendation();
                    recom.setItemId((int) itemId);
                    recom.setRecommendationItem((int) recommendation.getItemID());
                    recom.setSimilarity((double) recommendation.getValue());
                    allRecomms.add(recom);
                }
                x++;
            }
            recommendationRepo.saveAll(allRecomms);
            recommendationRepo.flush();
        } catch (IOException e) {
            System.out.println("There was an error.");
            e.printStackTrace();
        } catch (TasteException e) {
            System.out.println("There was a Taste Exception");
            e.printStackTrace();
        }
    }

    @PostMapping(path = "/recommendations")
    public  Iterator<Map.Entry<Integer,Double>> recommend(@RequestParam Integer uId){
        final List<Recommendation> all = recommendationRepo.findAll();
        List<UserExpansion> collect = userExpansions.findByUserId(uId);
        for(UserExpansion exp:collect){
            if(exp.getRating()==null)
                exp.setRating(0);
        }

        collect=collect.stream()
                .sorted(Comparator.comparing(UserExpansion::getRating).reversed())
                .collect(Collectors.toList());
        Map<Integer,List<Recommendation>> recommendationsForUser=new HashMap<Integer,List<Recommendation>>();
        for (UserExpansion userExpansion : collect) {
            List<Recommendation> tmpRecomm = all.stream()
                    .filter(c -> c.getItemId() == userExpansion.getExpansionId())
                    .sorted(Comparator.comparing(Recommendation::getSimilarity).reversed())
                    .collect(Collectors.toList());
            for (Recommendation recommendation : tmpRecomm) {
                if(!recommendationsForUser.containsKey(recommendation.getRecommendationItem()))
                    recommendationsForUser.put(recommendation.getRecommendationItem(),new ArrayList<>());
                recommendationsForUser.get(recommendation.getRecommendationItem()).add(recommendation);
            }
        }
        List<UserExpansion> finalCollect = collect;
        List<Integer> alreadyOwned = recommendationsForUser.keySet().stream()
                .filter(c -> finalCollect.stream().anyMatch(expan -> expan.getExpansionId() == c))
                .collect(Collectors.toList());
        for (Integer integer : alreadyOwned) {
            recommendationsForUser.remove(integer);
        }
        Map<Integer,Double> itemAverages=new HashMap<>();
        for (Integer recommendedItemID : recommendationsForUser.keySet()) {
            for (Recommendation recommendation : recommendationsForUser.get(recommendedItemID)) {
                recommendation.setSimilarity( ((recommendation.getSimilarity()+1.0)/2.0) *collect.
                        stream()
                        .filter(c->c.getExpansionId()==recommendation.getItemId())
                        .map(UserExpansion::getRating)
                        .collect(Collectors.toList()).get(0));
            }
             Double res= recommendationsForUser.get(recommendedItemID).stream()
                    .reduce(0.0,(partial,recom)->partial+recom.getSimilarity(),Double::sum);
             itemAverages.put(recommendedItemID,res);
        }

        return itemAverages.entrySet().stream()
                .sorted(Map.Entry.comparingByKey()).collect(Collectors.toList()).iterator();

    }
}
