package boardgames.backend.controllers;

import boardgames.backend.entities.ExternalReviewer;
import boardgames.backend.entities.ExternalReviewerWeight;
import boardgames.backend.entities.ExternalReviewerWeightPK;
import boardgames.backend.repositories.ExternalReviewerRepository;
import boardgames.backend.repositories.ExternalReviewerWeightRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(path = "/reviewers")
public class ExternalReviewerController {

    @Autowired
    ExternalReviewerRepository externalReviewerRepository;

    @Autowired
    private ExternalReviewerWeightRepository externalReviewerWeightRepository;

    @GetMapping(path = "/all")
    public List<ExternalReviewer> getAllExternalReviewers() {
        return externalReviewerRepository.findAll();
    }

    @PostMapping(path = "/edit")
    public String editUserExternalReviewerWeight(
            @RequestParam Integer userId,
            @RequestParam Integer externalReviewerId,
            @RequestParam Integer weight) {

        ExternalReviewerWeight externalReviewerWeight = new ExternalReviewerWeight();

        externalReviewerWeight.setUserId(userId);
        externalReviewerWeight.setExternalReviewerId(externalReviewerId);
        externalReviewerWeight.setWeight(weight);

        externalReviewerWeightRepository.save(externalReviewerWeight);

        return "Saved";
    }
}
