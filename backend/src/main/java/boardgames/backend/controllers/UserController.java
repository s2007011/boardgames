package boardgames.backend.controllers;

import boardgames.backend.entities.*;
import boardgames.backend.repositories.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@RestController
@RequestMapping(path = "/users")
public class UserController {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private ExternalReviewerRepository externalReviewerRepository;

    @Autowired
    private ExternalReviewerWeightRepository externalReviewerWeightRepository;

    @Autowired
    private UserExpansionRepository userExpansionRepository;

    @PostMapping(path = "/add")
    public @ResponseBody
    String addNewUser(@RequestParam String username,
                      @RequestParam String password,
                      @RequestParam Integer cityId,
                      @RequestParam(required = false) String name,
                      @RequestParam(required = false) String surname,
                      @RequestParam(required = false) String email,
                      @RequestParam(required = false) String phone,
                      @RequestParam(required = false) Integer age) {

        if (userRepository.findByUsername(username).size() == 1) {
            return "User exists";
        }

        User user = new User();
        user.setUsername(username);
        user.setPassword(password);
        user.setCityId(cityId);

        user.setName(name);
        user.setSurname(surname);
        user.setEmail(email);
        user.setPhone(phone);
        user.setAge(age);

        user.setUserReviewWeight(3);

        userRepository.save(user);

        int userId = userRepository.findByUsername(username).get(0).getId();

        for (ExternalReviewer externalReviewer : externalReviewerRepository.findAll()) {
            ExternalReviewerWeight externalReviewerWeight = new ExternalReviewerWeight();

            externalReviewerWeight.setExternalReviewerId(externalReviewer.getId());
            externalReviewerWeight.setUserId(userId);
            externalReviewerWeight.setWeight(3);

            externalReviewerWeightRepository.save(externalReviewerWeight);
        }

        return "Saved";
    }

    @PostMapping(path = "/validate")
    public @ResponseBody
    Integer userValidation(@RequestParam String username,
                           @RequestParam String password) {

        List<User> list = userRepository.findByUsernameAndPassword(username, password);

        if (list.size() == 1) {
            return list.get(0).getId();
        } else {
            return -1;
        }
    }

    @PostMapping(path = "/get")
    public User userValidation(@RequestParam int userId) {
        return userRepository.findById(userId).orElse(null);
    }

    @GetMapping(path = "/all")
    public @ResponseBody
    Iterable<User> getUsers() {
        return userRepository.findAll();
    }

    @PostMapping(path = "/expansions")
    public List<Expansion> getUserExpansions(@RequestParam int userId) {

        return userRepository.findById(userId)
                .map(user -> user
                        .getUserExpansionsById()
                        .stream()
                        .map(UserExpansion::getExpansionByExpansionId)
                        .collect(Collectors.toList())

                ).orElse(null);
    }

    @PostMapping(path = "/expansionIds")
    public List<Integer> getExpansionIds(@RequestParam int userId) {
        return getUserExpansions(userId).stream().map(Expansion::getId).collect(Collectors.toList());
    }

    @PostMapping(path = "/games")
    public List<Game> getUserGames(@RequestParam int userId) {

        return userRepository.findById(userId)
                .map(user -> user
                        .getUserExpansionsById()
                        .stream()
                        .map(UserExpansion::getExpansionByExpansionId)
                        .map(Expansion::getGameByGameId)
                        .collect(Collectors.toList())

                ).orElse(null);
    }

    @PostMapping(path = "/gameIds")
    public List<Integer> getUserGameIds(@RequestParam int userId) {
        return getUserGames(userId).stream().map(Game::getId).collect(Collectors.toList());
    }

    @PostMapping(path = "/externalReviewersWeights")
    public List<ExternalReviewerWeight> getExternalReviewerWeights(@RequestParam int userId) {
        return new ArrayList<>(userRepository.findById(userId).orElse(null)
                .getExternalReviewerWeightsById());
    }

    @PostMapping(path = "/externalReviewers")
    public List<ExternalReviewerWeight> getExternalReviewers(@RequestParam Integer userId) {

        return externalReviewerWeightRepository.findAllByUserId(userId);
    }

    @PostMapping(path = "/changeUserReviewWeight")
    public String changeUserReviewWeight(@RequestParam int userId, @RequestParam int userReviewWeight) {

        Optional<User> optionalUser = userRepository.findById(userId);

        if (optionalUser.isEmpty()) return "User does not exist";

        User user = optionalUser.get();

        user.setUserReviewWeight(userReviewWeight);

        userRepository.save(user);

        return "Success";
    }

    @PostMapping(path = "/addExpansion")
    public String addExpansion(@RequestParam int userId, @RequestParam int expansionId) {

        UserExpansion userExpansion = new UserExpansion();
        userExpansion.setUserId(userId);
        userExpansion.setExpansionId(expansionId);

        userExpansionRepository.save(userExpansion);

        return "Success";
    }

    @PostMapping(path = "/removeExpansion")
    public String removeExpansion(@RequestParam int userId, @RequestParam int expansionId) {

        UserExpansion userExpansion = new UserExpansion();
        userExpansion.setUserId(userId);
        userExpansion.setExpansionId(expansionId);

        userExpansionRepository.save(userExpansion);

        userExpansionRepository.delete(userExpansion);

        return "Success";
    }
}