package boardgames.backend.controllers;

import boardgames.backend.entities.UserExpansion;
import boardgames.backend.repositories.UserExpansionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@RestController
@RequestMapping(path = "/reviews")
public class UserExpansionController {

    private static class Review {

        private int userId;
        private int expansionId;
        private String username;
        private Integer rating;
        private String description;

        Review(UserExpansion userExpansion) {

            userId = userExpansion.getUserId();
            expansionId = userExpansion.getExpansionId();
            username = userExpansion.getUserByUserId().getUsername();
            rating = userExpansion.getRating();
            description = userExpansion.getReviewDescription();
        }

        public int getUserId() {
            return userId;
        }

        public void setUserId(int userId) {
            this.userId = userId;
        }

        public int getExpansionId() {
            return expansionId;
        }

        public void setExpansionId(int expansionId) {
            this.expansionId = expansionId;
        }

        public String getUsername() {
            return username;
        }

        public void setUsername(String username) {
            this.username = username;
        }

        public Integer getRating() {
            return rating;
        }

        public void setRating(Integer rating) {
            this.rating = rating;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }
    }

    @Autowired
    private UserExpansionRepository userExpansionRepository;

    @GetMapping(path = "/all")
    public List<Review> getReviews() {

        return userExpansionRepository.findAll().stream()
                .map(Review::new)
                .filter(review -> Objects.nonNull(review.description))
                .filter(review -> Objects.nonNull(review.rating))
                .collect(Collectors.toList());
    }
}
