package boardgames.backend.controllers;

import boardgames.backend.entities.*;
import boardgames.backend.repositories.LikeForReviewRepository;
import boardgames.backend.repositories.UserExpansionRepository;
import org.hibernate.dialect.MySQLDialect;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.web.bind.annotation.*;

import java.util.*;

@RestController
@RequestMapping(path = "/reviews")
public class ReviewController {


    @Autowired
    private UserExpansionRepository reviews;


    @Autowired
    private LikeForReviewRepository likes;

    @Autowired
    JdbcTemplate jdbcTemplate;

    @PostMapping(path = "/add")
    public @ResponseBody
    String addUpdReview(@RequestParam Integer uId,
                        @RequestParam Integer eId,
                        @RequestParam(required = false) Integer rating,
                        @RequestParam(required = false) String comment) {

        UserExpansion review = new UserExpansion();
        review.setExpansionId(eId);
        review.setUserId(uId);
        review.setRating(rating);
        review.setReviewDescription(comment);
        reviews.save(review);
        return "Saved";
    }

    /**
     * Only removes review comment
     *
     * @param uId pk part 1
     * @param eId pk part 2
     */
    @PostMapping(path = "/deleteComment")
    public void RemoveReviewComment(@RequestParam Integer uId,
                                    @RequestParam Integer eId) {

        UserExpansionPK userExpansionPK = new UserExpansionPK();
        userExpansionPK.setUserId(uId);
        userExpansionPK.setExpansionId(eId);
        if (reviews.findById(userExpansionPK).isPresent()) {
            UserExpansion userExpansion = reviews.findById(userExpansionPK).get();
            userExpansion.setReviewDescription(null);
            reviews.save(userExpansion);
        } else
            return;


    }

    /**
     * Only removes review comment
     *
     * @param uId pk part 1
     * @param eId pk part 2
     */
    @PostMapping(path = "/deleteRating")
    public void RemoveReviewRating(@RequestParam Integer uId,
                                   @RequestParam Integer eId) {
        UserExpansionPK userExpansionPK = new UserExpansionPK();
        userExpansionPK.setUserId(uId);
        userExpansionPK.setExpansionId(eId);
        if (reviews.findById(userExpansionPK).isPresent()) {
            UserExpansion userExpansion = reviews.findById(userExpansionPK).get();
            userExpansion.setRating(null);
            reviews.save(userExpansion);
        } else
            return;
    }

    SimpleJdbcCall jdbcCall;
    @PostMapping(path = "/topNReviews")
    public Iterator<Object> GetTopReviewsForGame(@RequestParam Integer eId,
                                                 @RequestParam Integer topN) {
        if(jdbcCall==null)
            jdbcCall= new SimpleJdbcCall(jdbcTemplate).withProcedureName("topReview");

        Map<String, Object> callParams = new HashMap<>();
        callParams.put("topN", topN);
        callParams.put("expancionId", eId);
        Map<String, Object> outputMap = jdbcCall.execute(callParams);

        return outputMap.values().iterator();
    }

    SimpleJdbcCall jdbcCall1;
    @PostMapping(path = "/GetExternalAndUserRatings")
    public Collection<Object> GetExternalAndUserRatings(@RequestParam Integer eId,
                                                        @RequestParam Integer uId) {
        if(jdbcCall1==null)
            jdbcCall1 = new SimpleJdbcCall(jdbcTemplate).withProcedureName("GetRatingsAndWeights");

        Map<String, Object> callParams = new HashMap<String, Object>();
        callParams.put("eId", eId);
        callParams.put("uId", uId);
        Map<String, Object> outputMap = jdbcCall1.execute(callParams);
        return outputMap.values();
    }


    //////////////////////// Likes for review below ////////////////////

    @PostMapping(path = "/addLike")
    public @ResponseBody
    String addReviewLike(@RequestParam Integer uId,
                         @RequestParam Integer revieweruId,
                         @RequestParam Integer eId,
                         @RequestParam Integer rating) {
        LikeForReview likeForReview = new LikeForReview();
        if (rating == 1)
            likeForReview.setLike((byte) 1);
        else
            likeForReview.setLike((byte) 0);
        likeForReview.setUserId(uId);
        likeForReview.setReviewerUserId(revieweruId);
        likeForReview.setReviewerExpansionId(eId);
        likes.save(likeForReview);
        return "Saved";
    }

    /**
     * Checks whether the user liked or disliked a specific review
     * @param userId
     * @param reviewerUserId
     * @param reviewerExpansionId
     * @return 0 no like/dislike, 1 dislike, 2 lke
     */
    @PostMapping(path="/isLiked")
    public Integer isLiked(@RequestParam  int userId,
                           @RequestParam  int reviewerUserId,
                           @RequestParam  int reviewerExpansionId){
        LikeForReviewPK like=new LikeForReviewPK();
        like.setUserId(userId);
        like.setReviewerUserId(reviewerUserId);
        like.setReviewerExpansionId(reviewerExpansionId);
        Optional<LikeForReview> byId = likes.findById(like);
        return byId.map(likeForReview -> likeForReview.getLike() + 1).orElse(0);
    }
    /**
     * Only removes review comment
     *
     * @param uId pk part 1
     * @param eId pk part 2
     */
    @DeleteMapping(path = "/deleteLike")
    public void RemoveLike(@RequestParam Integer uId,
                           @RequestParam Integer revieweruId,
                           @RequestParam Integer eId) {

        LikeForReviewPK likePK = new LikeForReviewPK();
        likePK.setUserId(uId);
        likePK.setReviewerExpansionId(eId);
        likePK.setReviewerUserId(revieweruId);
        if (likes.findById(likePK).isPresent())
            likes.delete(likes.findById(likePK).get());
    }
}
