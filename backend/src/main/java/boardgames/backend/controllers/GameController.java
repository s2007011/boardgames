package boardgames.backend.controllers;

import boardgames.backend.entities.Game;
import boardgames.backend.repositories.GameRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(path = "/games")
public class GameController {
    @Autowired
    private GameRepository gameRepository;

    @GetMapping(path = "/all")
    public Iterable<Game> getAllGames() {
        return gameRepository.findAll();
    }
}
