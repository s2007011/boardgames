package boardgames.backend.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.util.Collection;
import java.util.Objects;

@Entity
public class Club {
    private int id;
    private String name;
    private int cityId;
    private City cityByCityId;
	@JsonIgnore
    private Collection<ClubGenre> clubGenresById;
	@JsonIgnore
    private Collection<UserClub> userClubsById;

    @Id
    @Column(name = "id", nullable = false)
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "name", nullable = false, length = 45)
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Basic
    @Column(name = "city_id", nullable = false)
    public int getCityId() {
        return cityId;
    }

    public void setCityId(int cityId) {
        this.cityId = cityId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Club club = (Club) o;
        return id == club.id &&
                cityId == club.cityId &&
                Objects.equals(name, club.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, cityId);
    }

    @ManyToOne
    @JoinColumn( insertable=false,updatable=false,name = "city_id", referencedColumnName = "id", nullable = false)
    public City getCityByCityId() {
        return cityByCityId;
    }

    public void setCityByCityId(City cityByCityId) {
        this.cityByCityId = cityByCityId;
    }

    @OneToMany(mappedBy = "clubByClubId")
    public Collection<ClubGenre> getClubGenresById() {
        return clubGenresById;
    }

    public void setClubGenresById(Collection<ClubGenre> clubGenresById) {
        this.clubGenresById = clubGenresById;
    }

    @OneToMany(mappedBy = "clubByClubId")
    public Collection<UserClub> getUserClubsById() {
        return userClubsById;
    }

    public void setUserClubsById(Collection<UserClub> userClubsById) {
        this.userClubsById = userClubsById;
    }
}
