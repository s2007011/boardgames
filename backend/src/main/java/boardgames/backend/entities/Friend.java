package boardgames.backend.entities;

import javax.persistence.*;
import java.util.Objects;

@Entity
@IdClass(FriendPK.class)
public class Friend {
    private int userId1;
    private int userId2;
    private User userByUserId1;
    private User userByUserId2;

    @Id
    @Column(name = "user_id1", nullable = false)
    public int getUserId1() {
        return userId1;
    }

    public void setUserId1(int userId1) {
        this.userId1 = userId1;
    }

    @Id
    @Column(name = "user_id2", nullable = false)
    public int getUserId2() {
        return userId2;
    }

    public void setUserId2(int userId2) {
        this.userId2 = userId2;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Friend friend = (Friend) o;
        return userId1 == friend.userId1 &&
                userId2 == friend.userId2;
    }

    @Override
    public int hashCode() {
        return Objects.hash(userId1, userId2);
    }

    @ManyToOne
    @JoinColumn( insertable=false,updatable=false,name = "user_id1", referencedColumnName = "id", nullable = false)
    public User getUserByUserId1() {
        return userByUserId1;
    }

    public void setUserByUserId1(User userByUserId1) {
        this.userByUserId1 = userByUserId1;
    }

    @ManyToOne
    @JoinColumn( insertable=false,updatable=false,name = "user_id2", referencedColumnName = "id", nullable = false)
    public User getUserByUserId2() {
        return userByUserId2;
    }

    public void setUserByUserId2(User userByUserId2) {
        this.userByUserId2 = userByUserId2;
    }
}
