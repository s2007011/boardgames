package boardgames.backend.entities;

import javax.persistence.Column;
import javax.persistence.Id;
import java.io.Serializable;
import java.util.Objects;

public class ExternalReviewerExpansionPK implements Serializable {
    private int externalReviewerId;
    private int expansionId;

    @Column(name = "external_reviewer_id", nullable = false)
    @Id
    public int getExternalReviewerId() {
        return externalReviewerId;
    }

    public void setExternalReviewerId(int externalReviewerId) {
        this.externalReviewerId = externalReviewerId;
    }

    @Column(name = "expansion_id", nullable = false)
    @Id
    public int getExpansionId() {
        return expansionId;
    }

    public void setExpansionId(int expansionId) {
        this.expansionId = expansionId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ExternalReviewerExpansionPK that = (ExternalReviewerExpansionPK) o;
        return externalReviewerId == that.externalReviewerId &&
                expansionId == that.expansionId;
    }

    @Override
    public int hashCode() {
        return Objects.hash(externalReviewerId, expansionId);
    }
}
