package boardgames.backend.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.util.Collection;
import java.util.Objects;

@Entity
public class User {
    private int id;
    private String username;
    @JsonIgnore
    private String password;
    private String name;
    private String surname;
    private String email;
    private String phone;
    private Integer age;
    private Integer userReviewWeight;
    private int cityId;
	@JsonIgnore
    private Collection<ExternalReviewerWeight> externalReviewerWeightsById;
	@JsonIgnore
    private Collection<Friend> friendsById;
	@JsonIgnore
    private Collection<Friend> friendsById_0;
	@JsonIgnore
    private Collection<LikeForReview> likeForReviewsById;
	@JsonIgnore
    private Collection<Messages> messagesById;
	@JsonIgnore
    private Collection<Messages> messagesById_0;
	@JsonIgnore
    private City cityByCityId;
	@JsonIgnore
    private Collection<UserCard> userCardsById;
	@JsonIgnore
    private Collection<UserClub> userClubsById;
    @JsonIgnore
    private Collection<UserExpansion> userExpansionsById;

    @Id
    @Column(name = "id", nullable = false)
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "username", nullable = false, length = 45)
    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    @Basic
    @Column(name = "password", nullable = false, length = 45)
    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Basic
    @Column(name = "name", nullable = true, length = 45)
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Basic
    @Column(name = "surname", nullable = true, length = 45)
    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    @Basic
    @Column(name = "email", nullable = true, length = 45)
    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Basic
    @Column(name = "phone", nullable = true, length = 20)
    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    @Basic
    @Column(name = "age", nullable = true)
    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    @Basic
    @Column(name = "user_review_weight", nullable = true)
    public Integer getUserReviewWeight() {
        return userReviewWeight;
    }

    public void setUserReviewWeight(Integer userReviewWeight) {
        this.userReviewWeight = userReviewWeight;
    }

    @Basic
    @Column(name = "city_id", nullable = false)
    public int getCityId() {
        return cityId;
    }

    public void setCityId(int cityId) {
        this.cityId = cityId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        User user = (User) o;
        return id == user.id &&
                cityId == user.cityId &&
                Objects.equals(username, user.username) &&
                Objects.equals(password, user.password) &&
                Objects.equals(name, user.name) &&
                Objects.equals(surname, user.surname) &&
                Objects.equals(email, user.email) &&
                Objects.equals(phone, user.phone) &&
                Objects.equals(age, user.age) &&
                Objects.equals(userReviewWeight, user.userReviewWeight);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, username, password, name, surname, email, phone, age, userReviewWeight, cityId);
    }

    @OneToMany(mappedBy = "userByUserId")
    public Collection<ExternalReviewerWeight> getExternalReviewerWeightsById() {
        return externalReviewerWeightsById;
    }

    public void setExternalReviewerWeightsById(Collection<ExternalReviewerWeight> externalReviewerWeightsById) {
        this.externalReviewerWeightsById = externalReviewerWeightsById;
    }

    @OneToMany(mappedBy = "userByUserId1")
    public Collection<Friend> getFriendsById() {
        return friendsById;
    }

    public void setFriendsById(Collection<Friend> friendsById) {
        this.friendsById = friendsById;
    }

    @OneToMany(mappedBy = "userByUserId2")
    public Collection<Friend> getFriendsById_0() {
        return friendsById_0;
    }

    public void setFriendsById_0(Collection<Friend> friendsById_0) {
        this.friendsById_0 = friendsById_0;
    }

    @OneToMany(mappedBy = "userByUserId")
    public Collection<LikeForReview> getLikeForReviewsById() {
        return likeForReviewsById;
    }

    public void setLikeForReviewsById(Collection<LikeForReview> likeForReviewsById) {
        this.likeForReviewsById = likeForReviewsById;
    }

    @OneToMany(mappedBy = "userBySenderId")
    public Collection<Messages> getMessagesById() {
        return messagesById;
    }

    public void setMessagesById(Collection<Messages> messagesById) {
        this.messagesById = messagesById;
    }

    @OneToMany(mappedBy = "userByReceiverId")
    public Collection<Messages> getMessagesById_0() {
        return messagesById_0;
    }

    public void setMessagesById_0(Collection<Messages> messagesById_0) {
        this.messagesById_0 = messagesById_0;
    }

    @ManyToOne
    @JoinColumn( insertable=false,updatable=false,name = "city_id", referencedColumnName = "id", nullable = false)
    public City getCityByCityId() {
        return cityByCityId;
    }

    public void setCityByCityId(City cityByCityId) {
        this.cityByCityId = cityByCityId;
    }

    @OneToMany(mappedBy = "userByUserId")
    public Collection<UserCard> getUserCardsById() {
        return userCardsById;
    }

    public void setUserCardsById(Collection<UserCard> userCardsById) {
        this.userCardsById = userCardsById;
    }

    @OneToMany(mappedBy = "userByUserId")
    public Collection<UserClub> getUserClubsById() {
        return userClubsById;
    }

    public void setUserClubsById(Collection<UserClub> userClubsById) {
        this.userClubsById = userClubsById;
    }

    @OneToMany(mappedBy = "userByUserId")
    public Collection<UserExpansion> getUserExpansionsById() {
        return userExpansionsById;
    }

    public void setUserExpansionsById(Collection<UserExpansion> userExpansionsById) {
        this.userExpansionsById = userExpansionsById;
    }
}
