package boardgames.backend.entities;

import javax.persistence.Column;
import javax.persistence.Id;
import java.io.Serializable;
import java.util.Objects;

public class RecommendationPK implements Serializable {
    private int itemId;
    private int recommendationItem;

    @Column(name = "item_id", nullable = false)
    @Id
    public int getItemId() {
        return itemId;
    }

    public void setItemId(int itemId) {
        this.itemId = itemId;
    }

    @Column(name = "recommendation_item", nullable = false)
    @Id
    public int getRecommendationItem() {
        return recommendationItem;
    }

    public void setRecommendationItem(int recommendationItem) {
        this.recommendationItem = recommendationItem;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        RecommendationPK that = (RecommendationPK) o;
        return itemId == that.itemId &&
                recommendationItem == that.recommendationItem;
    }

    @Override
    public int hashCode() {
        return Objects.hash(itemId, recommendationItem);
    }
}
