package boardgames.backend.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.util.Collection;
import java.util.Objects;

@Entity
@Table(name = "external_reviewer", schema = "s2007066_project", catalog = "")
public class ExternalReviewer {
    private int id;
    private String name;
    private String url;
	@JsonIgnore
    private Collection<ExternalReviewerExpansion> externalReviewerExpansionsById;
	@JsonIgnore
    private Collection<ExternalReviewerWeight> externalReviewerWeightsById;

    @Id
    @Column(name = "id", nullable = false)
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "name", nullable = false, length = 45)
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Basic
    @Column(name = "url", nullable = true, length = 200)
    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ExternalReviewer that = (ExternalReviewer) o;
        return id == that.id &&
                Objects.equals(name, that.name) &&
                Objects.equals(url, that.url);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, url);
    }

    @OneToMany(mappedBy = "externalReviewerByExternalReviewerId")
    public Collection<ExternalReviewerExpansion> getExternalReviewerExpansionsById() {
        return externalReviewerExpansionsById;
    }

    public void setExternalReviewerExpansionsById(Collection<ExternalReviewerExpansion> externalReviewerExpansionsById) {
        this.externalReviewerExpansionsById = externalReviewerExpansionsById;
    }

    @OneToMany(mappedBy = "externalReviewer")
    public Collection<ExternalReviewerWeight> getExternalReviewerWeightsById() {
        return externalReviewerWeightsById;
    }

    public void setExternalReviewerWeightsById(Collection<ExternalReviewerWeight> externalReviewerWeightsById) {
        this.externalReviewerWeightsById = externalReviewerWeightsById;
    }
}
