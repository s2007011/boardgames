package boardgames.backend.entities;

import javax.persistence.Column;
import javax.persistence.Id;
import java.io.Serializable;
import java.util.Objects;

public class ClubGenrePK implements Serializable {
    private int genreId;
    private int clubId;

    @Column(name = "genre_id", nullable = false)
    @Id
    public int getGenreId() {
        return genreId;
    }

    public void setGenreId(int genreId) {
        this.genreId = genreId;
    }

    @Column(name = "club_id", nullable = false)
    @Id
    public int getClubId() {
        return clubId;
    }

    public void setClubId(int clubId) {
        this.clubId = clubId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ClubGenrePK that = (ClubGenrePK) o;
        return genreId == that.genreId &&
                clubId == that.clubId;
    }

    @Override
    public int hashCode() {
        return Objects.hash(genreId, clubId);
    }
}
