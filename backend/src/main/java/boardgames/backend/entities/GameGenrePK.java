package boardgames.backend.entities;

import javax.persistence.Column;
import javax.persistence.Id;
import java.io.Serializable;
import java.util.Objects;

public class GameGenrePK implements Serializable {
    private int gameId;
    private int genreId;

    @Column(name = "game_id", nullable = false)
    @Id
    public int getGameId() {
        return gameId;
    }

    public void setGameId(int gameId) {
        this.gameId = gameId;
    }

    @Column(name = "genre_id", nullable = false)
    @Id
    public int getGenreId() {
        return genreId;
    }

    public void setGenreId(int genreId) {
        this.genreId = genreId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        GameGenrePK that = (GameGenrePK) o;
        return gameId == that.gameId &&
                genreId == that.genreId;
    }

    @Override
    public int hashCode() {
        return Objects.hash(gameId, genreId);
    }
}
