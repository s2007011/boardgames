package boardgames.backend.entities;

import javax.persistence.Column;
import javax.persistence.Id;
import java.io.Serializable;
import java.util.Objects;

public class UserClubPK implements Serializable {
    private int userId;
    private int clubId;

    @Column(name = "user_id", nullable = false)
    @Id
    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    @Column(name = "club_id", nullable = false)
    @Id
    public int getClubId() {
        return clubId;
    }

    public void setClubId(int clubId) {
        this.clubId = clubId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        UserClubPK that = (UserClubPK) o;
        return userId == that.userId &&
                clubId == that.clubId;
    }

    @Override
    public int hashCode() {
        return Objects.hash(userId, clubId);
    }
}
