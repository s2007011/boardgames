package boardgames.backend.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.util.Collection;
import java.util.Objects;

@Entity
public class Expansion {
    private int id;
    private String name;
    @JsonIgnore
    private byte fixed;
    @JsonIgnore
    private int gameId;
	@JsonIgnore
    private Collection<Card> cardsById;
    @JsonIgnore
    private Game gameByGameId;
	@JsonIgnore
    private Collection<ExternalReviewerExpansion> externalReviewerExpansionsById;
	@JsonIgnore
    private Collection<RulesClarification> rulesClarificationsById;
	@JsonIgnore
    private Collection<Seller> sellersById;
	@JsonIgnore
    private Collection<UserExpansion> userExpansionsById;

    @Id
    @Column(name = "id", nullable = false)
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "name", nullable = false, length = 45)
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Basic
    @Column(name = "fixed", nullable = false)
    public byte getFixed() {
        return fixed;
    }

    public void setFixed(byte fixed) {
        this.fixed = fixed;
    }

    @Basic
    @Column(name = "game_id", nullable = false)
    public int getGameId() {
        return gameId;
    }

    public void setGameId(int gameId) {
        this.gameId = gameId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Expansion expansion = (Expansion) o;
        return id == expansion.id &&
                fixed == expansion.fixed &&
                gameId == expansion.gameId &&
                Objects.equals(name, expansion.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, fixed, gameId);
    }

    @OneToMany(mappedBy = "expansionByExpansionId")
    public Collection<Card> getCardsById() {
        return cardsById;
    }

    public void setCardsById(Collection<Card> cardsById) {
        this.cardsById = cardsById;
    }

    @ManyToOne
    @JoinColumn( insertable=false,updatable=false,name = "game_id", referencedColumnName = "id", nullable = false)
    public Game getGameByGameId() {
        return gameByGameId;
    }

    public void setGameByGameId(Game gameByGameId) {
        this.gameByGameId = gameByGameId;
    }

    @OneToMany(mappedBy = "expansionByExpansionId")
    public Collection<ExternalReviewerExpansion> getExternalReviewerExpansionsById() {
        return externalReviewerExpansionsById;
    }

    public void setExternalReviewerExpansionsById(Collection<ExternalReviewerExpansion> externalReviewerExpansionsById) {
        this.externalReviewerExpansionsById = externalReviewerExpansionsById;
    }

    @OneToMany(mappedBy = "expansionByExpansionId")
    public Collection<RulesClarification> getRulesClarificationsById() {
        return rulesClarificationsById;
    }

    public void setRulesClarificationsById(Collection<RulesClarification> rulesClarificationsById) {
        this.rulesClarificationsById = rulesClarificationsById;
    }

    @OneToMany(mappedBy = "expansionByExpansionId")
    public Collection<Seller> getSellersById() {
        return sellersById;
    }

    public void setSellersById(Collection<Seller> sellersById) {
        this.sellersById = sellersById;
    }

    @OneToMany(mappedBy = "expansionByExpansionId")
    public Collection<UserExpansion> getUserExpansionsById() {
        return userExpansionsById;
    }

    public void setUserExpansionsById(Collection<UserExpansion> userExpansionsById) {
        this.userExpansionsById = userExpansionsById;
    }
}
