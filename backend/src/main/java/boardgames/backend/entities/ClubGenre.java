package boardgames.backend.entities;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "club_genre", schema = "s2007066_project", catalog = "")
@IdClass(ClubGenrePK.class)
public class ClubGenre {
    private int genreId;
    private int clubId;
    private Genre genreByGenreId;
    private Club clubByClubId;

    @Id
    @Column(name = "genre_id", nullable = false)
    public int getGenreId() {
        return genreId;
    }

    public void setGenreId(int genreId) {
        this.genreId = genreId;
    }

    @Id
    @Column(name = "club_id", nullable = false)
    public int getClubId() {
        return clubId;
    }

    public void setClubId(int clubId) {
        this.clubId = clubId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ClubGenre clubGenre = (ClubGenre) o;
        return genreId == clubGenre.genreId &&
                clubId == clubGenre.clubId;
    }

    @Override
    public int hashCode() {
        return Objects.hash(genreId, clubId);
    }

    @ManyToOne
    @JoinColumn( insertable=false,updatable=false,name = "genre_id", referencedColumnName = "id", nullable = false)
    public Genre getGenreByGenreId() {
        return genreByGenreId;
    }

    public void setGenreByGenreId(Genre genreByGenreId) {
        this.genreByGenreId = genreByGenreId;
    }

    @ManyToOne
    @JoinColumn( insertable=false,updatable=false,name = "club_id", referencedColumnName = "id", nullable = false)
    public Club getClubByClubId() {
        return clubByClubId;
    }

    public void setClubByClubId(Club clubByClubId) {
        this.clubByClubId = clubByClubId;
    }
}
