package boardgames.backend.entities;

import javax.persistence.Column;
import javax.persistence.Id;
import java.io.Serializable;
import java.util.Objects;

public class ExternalReviewerWeightPK implements Serializable {
    private int userId;
    private int externalReviewerId;

    @Column(name = "user_id", nullable = false)
    @Id
    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    @Column(name = "external_reviewer_id", nullable = false)
    @Id
    public int getExternalReviewerId() {
        return externalReviewerId;
    }

    public void setExternalReviewerId(int externalReviewerId) {
        this.externalReviewerId = externalReviewerId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ExternalReviewerWeightPK that = (ExternalReviewerWeightPK) o;
        return userId == that.userId &&
                externalReviewerId == that.externalReviewerId;
    }

    @Override
    public int hashCode() {
        return Objects.hash(userId, externalReviewerId);
    }
}
