package boardgames.backend.entities;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "external_reviewer_expansion", schema = "s2007066_project", catalog = "")
@IdClass(ExternalReviewerExpansionPK.class)
public class ExternalReviewerExpansion {
    private int externalReviewerId;
    private int expansionId;
    private int rating;
    private String reviewDescription;
    private ExternalReviewer externalReviewerByExternalReviewerId;
    private Expansion expansionByExpansionId;

    @Id
    @Column(name = "external_reviewer_id", nullable = false)
    public int getExternalReviewerId() {
        return externalReviewerId;
    }

    public void setExternalReviewerId(int externalReviewerId) {
        this.externalReviewerId = externalReviewerId;
    }

    @Id
    @Column(name = "expansion_id", nullable = false)
    public int getExpansionId() {
        return expansionId;
    }

    public void setExpansionId(int expansionId) {
        this.expansionId = expansionId;
    }

    @Basic
    @Column(name = "rating", nullable = false)
    public int getRating() {
        return rating;
    }

    public void setRating(int rating) {
        this.rating = rating;
    }

    @Basic
    @Column(name = "review_description", nullable = true, length = 200)
    public String getReviewDescription() {
        return reviewDescription;
    }

    public void setReviewDescription(String reviewDescription) {
        this.reviewDescription = reviewDescription;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ExternalReviewerExpansion that = (ExternalReviewerExpansion) o;
        return externalReviewerId == that.externalReviewerId &&
                expansionId == that.expansionId &&
                rating == that.rating &&
                Objects.equals(reviewDescription, that.reviewDescription);
    }

    @Override
    public int hashCode() {
        return Objects.hash(externalReviewerId, expansionId, rating, reviewDescription);
    }

    @ManyToOne
    @JoinColumn( insertable=false,updatable=false,name = "external_reviewer_id", referencedColumnName = "id", nullable = false)
    public ExternalReviewer getExternalReviewerByExternalReviewerId() {
        return externalReviewerByExternalReviewerId;
    }

    public void setExternalReviewerByExternalReviewerId(ExternalReviewer externalReviewerByExternalReviewerId) {
        this.externalReviewerByExternalReviewerId = externalReviewerByExternalReviewerId;
    }

    @ManyToOne
    @JoinColumn( insertable=false,updatable=false,name = "expansion_id", referencedColumnName = "id", nullable = false)
    public Expansion getExpansionByExpansionId() {
        return expansionByExpansionId;
    }

    public void setExpansionByExpansionId(Expansion expansionByExpansionId) {
        this.expansionByExpansionId = expansionByExpansionId;
    }
}
