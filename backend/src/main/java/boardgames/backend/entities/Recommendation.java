package boardgames.backend.entities;

import javax.persistence.*;
import java.util.Objects;

@Entity
@IdClass(RecommendationPK.class)
public class Recommendation {
    private int itemId;
    private int recommendationItem;
    private Double similarity;

    @Id
    @Column(name = "item_id", nullable = false)
    public int getItemId() {
        return itemId;
    }

    public void setItemId(int itemId) {
        this.itemId = itemId;
    }

    @Id
    @Column(name = "recommendation_item", nullable = false)
    public int getRecommendationItem() {
        return recommendationItem;
    }

    public void setRecommendationItem(int recommendationItem) {
        this.recommendationItem = recommendationItem;
    }

    @Basic
    @Column(name = "similarity", nullable = true, precision = 0)
    public Double getSimilarity() {
        return similarity;
    }

    public void setSimilarity(Double similarity) {
        this.similarity = similarity;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Recommendation that = (Recommendation) o;
        return itemId == that.itemId &&
                recommendationItem == that.recommendationItem &&
                Objects.equals(similarity, that.similarity);
    }

    @Override
    public int hashCode() {
        return Objects.hash(itemId, recommendationItem, similarity);
    }
}
