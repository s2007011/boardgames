package boardgames.backend.entities;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Objects;

@Entity
public class Messages {
    private int id;
    private String message;
    private Timestamp time;
    private int senderId;
    private int receiverId;
    private User userBySenderId;
    private User userByReceiverId;

    @Id
    @Column(name = "id", nullable = false)
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "message", nullable = false, length = 100)
    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    @Basic
    @Column(name = "time", nullable = false)
    public Timestamp getTime() {
        return time;
    }

    public void setTime(Timestamp time) {
        this.time = time;
    }

    @Basic
    @Column(name = "sender_id", nullable = false)
    public int getSenderId() {
        return senderId;
    }

    public void setSenderId(int senderId) {
        this.senderId = senderId;
    }

    @Basic
    @Column(name = "receiver_id", nullable = false)
    public int getReceiverId() {
        return receiverId;
    }

    public void setReceiverId(int receiverId) {
        this.receiverId = receiverId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Messages messages = (Messages) o;
        return id == messages.id &&
                senderId == messages.senderId &&
                receiverId == messages.receiverId &&
                Objects.equals(message, messages.message) &&
                Objects.equals(time, messages.time);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, message, time, senderId, receiverId);
    }

    @ManyToOne
    @JoinColumn( insertable=false,updatable=false,name = "sender_id", referencedColumnName = "id", nullable = false)
    public User getUserBySenderId() {
        return userBySenderId;
    }

    public void setUserBySenderId(User userBySenderId) {
        this.userBySenderId = userBySenderId;
    }

    @ManyToOne
    @JoinColumn( insertable=false,updatable=false,name = "receiver_id", referencedColumnName = "id", nullable = false)
    public User getUserByReceiverId() {
        return userByReceiverId;
    }

    public void setUserByReceiverId(User userByReceiverId) {
        this.userByReceiverId = userByReceiverId;
    }
}
