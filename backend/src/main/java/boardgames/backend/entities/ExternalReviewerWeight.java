package boardgames.backend.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "external_reviewer_weight", schema = "s2007066_project", catalog = "")
@IdClass(ExternalReviewerWeightPK.class)
public class ExternalReviewerWeight {
    private int userId;
    private int externalReviewerId;
    private Integer weight;
    @JsonIgnore
    private User userByUserId;
    private ExternalReviewer externalReviewer;

    @Id
    @Column(name = "user_id", nullable = false)
    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    @Id
    @Column(name = "external_reviewer_id", nullable = false)
    public int getExternalReviewerId() {
        return externalReviewerId;
    }

    public void setExternalReviewerId(int externalReviewerId) {
        this.externalReviewerId = externalReviewerId;
    }

    @Basic
    @Column(name = "weight", nullable = true)
    public Integer getWeight() {
        return weight;
    }

    public void setWeight(Integer weight) {
        this.weight = weight;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ExternalReviewerWeight that = (ExternalReviewerWeight) o;
        return userId == that.userId &&
                externalReviewerId == that.externalReviewerId &&
                Objects.equals(weight, that.weight);
    }

    @Override
    public int hashCode() {
        return Objects.hash(userId, externalReviewerId, weight);
    }

    @ManyToOne
    @JoinColumn( insertable=false,updatable=false,name = "user_id", referencedColumnName = "id", nullable = false)
    public User getUserByUserId() {
        return userByUserId;
    }

    public void setUserByUserId(User userByUserId) {
        this.userByUserId = userByUserId;
    }

    @ManyToOne
    @JoinColumn( insertable=false,updatable=false,name = "external_reviewer_id", referencedColumnName = "id", nullable = false)
    public ExternalReviewer getExternalReviewer() {
        return externalReviewer;
    }

    public void setExternalReviewer(ExternalReviewer externalReviewerByExternalReviewerId) {
        this.externalReviewer = externalReviewerByExternalReviewerId;
    }
}
