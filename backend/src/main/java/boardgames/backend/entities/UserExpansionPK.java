package boardgames.backend.entities;

import javax.persistence.Column;
import javax.persistence.Id;
import java.io.Serializable;
import java.util.Objects;

public class UserExpansionPK implements Serializable {
    private int userId;
    private int expansionId;

    @Column(name = "user_id", nullable = false)
    @Id
    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    @Column(name = "expansion_id", nullable = false)
    @Id
    public int getExpansionId() {
        return expansionId;
    }

    public void setExpansionId(int expansionId) {
        this.expansionId = expansionId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        UserExpansionPK that = (UserExpansionPK) o;
        return userId == that.userId &&
                expansionId == that.expansionId;
    }

    @Override
    public int hashCode() {
        return Objects.hash(userId, expansionId);
    }
}
