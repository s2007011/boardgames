package boardgames.backend.entities;

import javax.persistence.Column;
import javax.persistence.Id;
import java.io.Serializable;
import java.util.Objects;

public class FriendPK implements Serializable {
    private int userId1;
    private int userId2;

    @Column(name = "user_id1", nullable = false)
    @Id
    public int getUserId1() {
        return userId1;
    }

    public void setUserId1(int userId1) {
        this.userId1 = userId1;
    }

    @Column(name = "user_id2", nullable = false)
    @Id
    public int getUserId2() {
        return userId2;
    }

    public void setUserId2(int userId2) {
        this.userId2 = userId2;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        FriendPK friendPK = (FriendPK) o;
        return userId1 == friendPK.userId1 &&
                userId2 == friendPK.userId2;
    }

    @Override
    public int hashCode() {
        return Objects.hash(userId1, userId2);
    }
}
