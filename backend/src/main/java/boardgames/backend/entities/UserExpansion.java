package boardgames.backend.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.util.Collection;
import java.util.Objects;

@Entity
@Table(name = "user_expansion", schema = "s2007066_project", catalog = "")
@IdClass(UserExpansionPK.class)
public class UserExpansion {
    private int userId;
    private int expansionId;
    private Integer rating;
    private String reviewDescription;
    private String houseRules;
	@JsonIgnore
    private Collection<LikeForReview> likeForReviews;
	@JsonIgnore
    private User userByUserId;
	@JsonIgnore
    private Expansion expansionByExpansionId;

    @Id
    @Column(name = "user_id", nullable = false)
    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    @Id
    @Column(name = "expansion_id", nullable = false)
    public int getExpansionId() {
        return expansionId;
    }

    public void setExpansionId(int expansionId) {
        this.expansionId = expansionId;
    }

    @Basic
    @Column(name = "rating", nullable = true)
    public Integer getRating() {
        return rating;
    }

    public void setRating(Integer rating) {
        this.rating = rating;
    }

    @Basic
    @Column(name = "review_description", nullable = true, length = 200)
    public String getReviewDescription() {
        return reviewDescription;
    }

    public void setReviewDescription(String reviewDescription) {
        this.reviewDescription = reviewDescription;
    }

    @Basic
    @Column(name = "house_rules", nullable = true, length = 200)
    public String getHouseRules() {
        return houseRules;
    }

    public void setHouseRules(String houseRules) {
        this.houseRules = houseRules;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        UserExpansion that = (UserExpansion) o;
        return userId == that.userId &&
                expansionId == that.expansionId &&
                Objects.equals(rating, that.rating) &&
                Objects.equals(reviewDescription, that.reviewDescription) &&
                Objects.equals(houseRules, that.houseRules);
    }

    @Override
    public int hashCode() {
        return Objects.hash(userId, expansionId, rating, reviewDescription, houseRules);
    }

    @OneToMany(mappedBy = "userExpansion")
    public Collection<LikeForReview> getLikeForReviews() {
        return likeForReviews;
    }

    public void setLikeForReviews(Collection<LikeForReview> likeForReviews) {
        this.likeForReviews = likeForReviews;
    }

    @ManyToOne
    @JoinColumn( insertable=false,updatable=false,name = "user_id", referencedColumnName = "id", nullable = false)
    public User getUserByUserId() {
        return userByUserId;
    }

    public void setUserByUserId(User userByUserId) {
        this.userByUserId = userByUserId;
    }

    @ManyToOne
    @JoinColumn( insertable=false,updatable=false,name = "expansion_id", referencedColumnName = "id", nullable = false)
    public Expansion getExpansionByExpansionId() {
        return expansionByExpansionId;
    }

    public void setExpansionByExpansionId(Expansion expansionByExpansionId) {
        this.expansionByExpansionId = expansionByExpansionId;
    }
}
