package boardgames.backend.entities;

import javax.persistence.Column;
import javax.persistence.Id;
import java.io.Serializable;
import java.util.Objects;

public class LikeForReviewPK implements Serializable {
    private int userId;
    private int reviewerUserId;
    private int reviewerExpansionId;

    @Column(name = "user_id", nullable = false)
    @Id
    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    @Column(name = "reviewer_user_id", nullable = false)
    @Id
    public int getReviewerUserId() {
        return reviewerUserId;
    }

    public void setReviewerUserId(int reviewerUserId) {
        this.reviewerUserId = reviewerUserId;
    }

    @Column(name = "reviewer_expansion_id", nullable = false)
    @Id
    public int getReviewerExpansionId() {
        return reviewerExpansionId;
    }

    public void setReviewerExpansionId(int reviewerExpansionId) {
        this.reviewerExpansionId = reviewerExpansionId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        LikeForReviewPK that = (LikeForReviewPK) o;
        return userId == that.userId &&
                reviewerUserId == that.reviewerUserId &&
                reviewerExpansionId == that.reviewerExpansionId;
    }

    @Override
    public int hashCode() {
        return Objects.hash(userId, reviewerUserId, reviewerExpansionId);
    }
}
