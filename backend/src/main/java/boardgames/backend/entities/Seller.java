package boardgames.backend.entities;

import javax.persistence.*;
import java.util.Objects;

@Entity
public class Seller {
    private int id;
    private String name;
    private double price;
    private String condition;
    private String url;
    private int expansionId;
    private Expansion expansionByExpansionId;

    @Id
    @Column(name = "id", nullable = false)
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "name", nullable = false, length = 45)
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Basic
    @Column(name = "price", nullable = false, precision = 0)
    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    @Basic
    @Column(name = "condition", nullable = true, length = 4)
    public String getCondition() {
        return condition;
    }

    public void setCondition(String condition) {
        this.condition = condition;
    }

    @Basic
    @Column(name = "url", nullable = false, length = 200)
    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    @Basic
    @Column(name = "expansion_id", nullable = false)
    public int getExpansionId() {
        return expansionId;
    }

    public void setExpansionId(int expansionId) {
        this.expansionId = expansionId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Seller seller = (Seller) o;
        return id == seller.id &&
                Double.compare(seller.price, price) == 0 &&
                expansionId == seller.expansionId &&
                Objects.equals(name, seller.name) &&
                Objects.equals(condition, seller.condition) &&
                Objects.equals(url, seller.url);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, price, condition, url, expansionId);
    }

    @ManyToOne
    @JoinColumn( insertable=false,updatable=false,name = "expansion_id", referencedColumnName = "id", nullable = false)
    public Expansion getExpansionByExpansionId() {
        return expansionByExpansionId;
    }

    public void setExpansionByExpansionId(Expansion expansionByExpansionId) {
        this.expansionByExpansionId = expansionByExpansionId;
    }
}
