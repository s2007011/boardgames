package boardgames.backend.entities;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "like_for_review", schema = "s2007066_project", catalog = "")
@IdClass(LikeForReviewPK.class)
public class LikeForReview {
    private int userId;
    private int reviewerUserId;
    private int reviewerExpansionId;
    private byte like;
    private User userByUserId;
    private UserExpansion userExpansion;

    @Id
    @Column(name = "user_id", nullable = false)
    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    @Id
    @Column(name = "reviewer_user_id", nullable = false)
    public int getReviewerUserId() {
        return reviewerUserId;
    }

    public void setReviewerUserId(int reviewerUserId) {
        this.reviewerUserId = reviewerUserId;
    }

    @Id
    @Column(name = "reviewer_expansion_id", nullable = false)
    public int getReviewerExpansionId() {
        return reviewerExpansionId;
    }

    public void setReviewerExpansionId(int reviewerExpansionId) {
        this.reviewerExpansionId = reviewerExpansionId;
    }

    @Basic
    @Column(name = "`like`", nullable = false)
    public byte getLike() {
        return like;
    }

    public void setLike(byte like) {
        this.like = like;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        LikeForReview that = (LikeForReview) o;
        return userId == that.userId &&
                reviewerUserId == that.reviewerUserId &&
                reviewerExpansionId == that.reviewerExpansionId &&
                like == that.like;
    }

    @Override
    public int hashCode() {
        return Objects.hash(userId, reviewerUserId, reviewerExpansionId, like);
    }

    @ManyToOne
    @JoinColumn( insertable=false,updatable=false,name = "user_id", referencedColumnName = "id", nullable = false)
    public User getUserByUserId() {
        return userByUserId;
    }

    public void setUserByUserId(User userByUserId) {
        this.userByUserId = userByUserId;
    }

    @ManyToOne
    @JoinColumns({@JoinColumn( insertable=false,updatable=false,name = "reviewer_user_id", referencedColumnName = "user_id", nullable = false), @JoinColumn( insertable=false,updatable=false,name = "reviewer_expansion_id", referencedColumnName = "expansion_id", nullable = false)})
    public UserExpansion getUserExpansion() {
        return userExpansion;
    }

    public void setUserExpansion(UserExpansion userExpansion) {
        this.userExpansion = userExpansion;
    }
}
