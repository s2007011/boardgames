package boardgames.backend.entities;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "user_card", schema = "s2007066_project", catalog = "")
@IdClass(UserCardPK.class)
public class UserCard {
    private int userId;
    private int cardId;
    private Integer quantity;
    private User userByUserId;
    private Card cardByCardId;

    @Id
    @Column(name = "user_id", nullable = false)
    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    @Id
    @Column(name = "card_id", nullable = false)
    public int getCardId() {
        return cardId;
    }

    public void setCardId(int cardId) {
        this.cardId = cardId;
    }

    @Basic
    @Column(name = "quantity", nullable = true)
    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        UserCard userCard = (UserCard) o;
        return userId == userCard.userId &&
                cardId == userCard.cardId &&
                Objects.equals(quantity, userCard.quantity);
    }

    @Override
    public int hashCode() {
        return Objects.hash(userId, cardId, quantity);
    }

    @ManyToOne
    @JoinColumn( insertable=false,updatable=false,name = "user_id", referencedColumnName = "id", nullable = false)
    public User getUserByUserId() {
        return userByUserId;
    }

    public void setUserByUserId(User userByUserId) {
        this.userByUserId = userByUserId;
    }

    @ManyToOne
    @JoinColumn( insertable=false,updatable=false,name = "card_id", referencedColumnName = "id", nullable = false)
    public Card getCardByCardId() {
        return cardByCardId;
    }

    public void setCardByCardId(Card cardByCardId) {
        this.cardByCardId = cardByCardId;
    }
}
