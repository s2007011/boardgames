package boardgames.backend.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.util.Collection;
import java.util.Objects;

@Entity
public class Game {
    private int id;
    private String name;
    private Integer minNoOfPlayers;
    private Integer maxNoOfPlayers;
    private String imageUrl;
//	@JsonIgnore
    private Collection<Expansion> expansions;
	@JsonIgnore
    private Collection<GameGenre> gameGenresById;

    @Id
    @Column(name = "id", nullable = false)
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "name", nullable = false, length = 45)
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Basic
    @Column(name = "min_no_of_players", nullable = true)
    public Integer getMinNoOfPlayers() {
        return minNoOfPlayers;
    }

    public void setMinNoOfPlayers(Integer minNoOfPlayers) {
        this.minNoOfPlayers = minNoOfPlayers;
    }

    @Basic
    @Column(name = "max_no_of_players", nullable = true)
    public Integer getMaxNoOfPlayers() {
        return maxNoOfPlayers;
    }

    public void setMaxNoOfPlayers(Integer maxNoOfPlayers) {
        this.maxNoOfPlayers = maxNoOfPlayers;
    }

    @Basic
    @Column(name = "image_url", nullable = true, length = 200)
    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Game game = (Game) o;
        return id == game.id &&
                Objects.equals(name, game.name) &&
                Objects.equals(minNoOfPlayers, game.minNoOfPlayers) &&
                Objects.equals(maxNoOfPlayers, game.maxNoOfPlayers) &&
                Objects.equals(imageUrl, game.imageUrl);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, minNoOfPlayers, maxNoOfPlayers, imageUrl);
    }

    @OneToMany(mappedBy = "gameByGameId")
    public Collection<Expansion> getExpansions() {
        return expansions;
    }

    public void setExpansions(Collection<Expansion> expansions) {
        this.expansions = expansions;
    }

    @OneToMany(mappedBy = "gameByGameId")
    public Collection<GameGenre> getGameGenresById() {
        return gameGenresById;
    }

    public void setGameGenresById(Collection<GameGenre> gameGenresById) {
        this.gameGenresById = gameGenresById;
    }
}
