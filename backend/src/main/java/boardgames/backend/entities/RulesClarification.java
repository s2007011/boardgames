package boardgames.backend.entities;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "rules_clarification", schema = "s2007066_project", catalog = "")
public class RulesClarification {
    private int id;
    private String type;
    private String description;
    private int expansionId;
    private Expansion expansionByExpansionId;

    @Id
    @Column(name = "id", nullable = false)
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "type", nullable = false, length = 10)
    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    @Basic
    @Column(name = "description", nullable = true, length = 200)
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Basic
    @Column(name = "expansion_id", nullable = false)
    public int getExpansionId() {
        return expansionId;
    }

    public void setExpansionId(int expansionId) {
        this.expansionId = expansionId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        RulesClarification that = (RulesClarification) o;
        return id == that.id &&
                expansionId == that.expansionId &&
                Objects.equals(type, that.type) &&
                Objects.equals(description, that.description);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, type, description, expansionId);
    }

    @ManyToOne
    @JoinColumn( insertable=false,updatable=false,name = "expansion_id", referencedColumnName = "id", nullable = false)
    public Expansion getExpansionByExpansionId() {
        return expansionByExpansionId;
    }

    public void setExpansionByExpansionId(Expansion expansionByExpansionId) {
        this.expansionByExpansionId = expansionByExpansionId;
    }
}
