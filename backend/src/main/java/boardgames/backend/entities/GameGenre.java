package boardgames.backend.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "game_genre", schema = "s2007066_project", catalog = "")
@IdClass(GameGenrePK.class)
public class GameGenre {
    private int gameId;
    private int genreId;
    @JsonIgnore
    private Game gameByGameId;
    @JsonIgnore
    private Genre genreByGenreId;

    @Id
    @Column(name = "game_id", nullable = false)
    public int getGameId() {
        return gameId;
    }

    public void setGameId(int gameId) {
        this.gameId = gameId;
    }

    @Id
    @Column(name = "genre_id", nullable = false)
    public int getGenreId() {
        return genreId;
    }

    public void setGenreId(int genreId) {
        this.genreId = genreId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        GameGenre gameGenre = (GameGenre) o;
        return gameId == gameGenre.gameId &&
                genreId == gameGenre.genreId;
    }

    @Override
    public int hashCode() {
        return Objects.hash(gameId, genreId);
    }

    @ManyToOne
    @JoinColumn( insertable=false,updatable=false,name = "game_id", referencedColumnName = "id", nullable = false)
    public Game getGameByGameId() {
        return gameByGameId;
    }

    public void setGameByGameId(Game gameByGameId) {
        this.gameByGameId = gameByGameId;
    }

    @ManyToOne
    @JoinColumn( insertable=false,updatable=false,name = "genre_id", referencedColumnName = "id", nullable = false)
    public Genre getGenreByGenreId() {
        return genreByGenreId;
    }

    public void setGenreByGenreId(Genre genreByGenreId) {
        this.genreByGenreId = genreByGenreId;
    }
}
