package boardgames.backend.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.util.Collection;
import java.util.Objects;

@Entity
public class Card {
    private int id;
    private String imgUrl;
    private int expansionId;
    private Expansion expansionByExpansionId;
	@JsonIgnore
    private Collection<UserCard> userCardsById;

    @Id
    @Column(name = "id", nullable = false)
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "img_url", nullable = true, length = 200)
    public String getImgUrl() {
        return imgUrl;
    }

    public void setImgUrl(String imgUrl) {
        this.imgUrl = imgUrl;
    }

    @Basic
    @Column(name = "expansion_id", nullable = false)
    public int getExpansionId() {
        return expansionId;
    }

    public void setExpansionId(int expansionId) {
        this.expansionId = expansionId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Card card = (Card) o;
        return id == card.id &&
                expansionId == card.expansionId &&
                Objects.equals(imgUrl, card.imgUrl);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, imgUrl, expansionId);
    }

    @ManyToOne
    @JoinColumn( insertable=false,updatable=false,name = "expansion_id", referencedColumnName = "id", nullable = false)
    public Expansion getExpansionByExpansionId() {
        return expansionByExpansionId;
    }

    public void setExpansionByExpansionId(Expansion expansionByExpansionId) {
        this.expansionByExpansionId = expansionByExpansionId;
    }

    @OneToMany(mappedBy = "cardByCardId")
    public Collection<UserCard> getUserCardsById() {
        return userCardsById;
    }

    public void setUserCardsById(Collection<UserCard> userCardsById) {
        this.userCardsById = userCardsById;
    }
}
