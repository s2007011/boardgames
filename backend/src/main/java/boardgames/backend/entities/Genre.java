package boardgames.backend.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.util.Collection;
import java.util.Objects;

@Entity
public class Genre {
    private int id;
    private String name;
	@JsonIgnore
    private Collection<ClubGenre> clubGenresById;
	@JsonIgnore
    private Collection<GameGenre> gameGenresById;

    @Id
    @Column(name = "id", nullable = false)
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "name", nullable = false, length = 45)
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Genre genre = (Genre) o;
        return id == genre.id &&
                Objects.equals(name, genre.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name);
    }

    @OneToMany(mappedBy = "genreByGenreId")
    public Collection<ClubGenre> getClubGenresById() {
        return clubGenresById;
    }

    public void setClubGenresById(Collection<ClubGenre> clubGenresById) {
        this.clubGenresById = clubGenresById;
    }

    @OneToMany(mappedBy = "genreByGenreId")
    public Collection<GameGenre> getGameGenresById() {
        return gameGenresById;
    }

    public void setGameGenresById(Collection<GameGenre> gameGenresById) {
        this.gameGenresById = gameGenresById;
    }
}
