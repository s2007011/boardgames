package boardgames.backend.entities;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "user_club", schema = "s2007066_project", catalog = "")
@IdClass(UserClubPK.class)
public class UserClub {
    private int userId;
    private int clubId;
    private User userByUserId;
    private Club clubByClubId;

    @Id
    @Column(name = "user_id", nullable = false)
    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    @Id
    @Column(name = "club_id", nullable = false)
    public int getClubId() {
        return clubId;
    }

    public void setClubId(int clubId) {
        this.clubId = clubId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        UserClub userClub = (UserClub) o;
        return userId == userClub.userId &&
                clubId == userClub.clubId;
    }

    @Override
    public int hashCode() {
        return Objects.hash(userId, clubId);
    }

    @ManyToOne
    @JoinColumn( insertable=false,updatable=false,name = "user_id", referencedColumnName = "id", nullable = false)
    public User getUserByUserId() {
        return userByUserId;
    }

    public void setUserByUserId(User userByUserId) {
        this.userByUserId = userByUserId;
    }

    @ManyToOne
    @JoinColumn( insertable=false,updatable=false,name = "club_id", referencedColumnName = "id", nullable = false)
    public Club getClubByClubId() {
        return clubByClubId;
    }

    public void setClubByClubId(Club clubByClubId) {
        this.clubByClubId = clubByClubId;
    }
}
